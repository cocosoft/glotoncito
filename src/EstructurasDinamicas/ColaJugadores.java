package EstructurasDinamicas;
/*Esta estructura manipula los Jugadores para que est�n siempre en un orden
 espec�fico */
import Jugador.*;

public class ColaJugadores {
    
    private NodoParticipante primero;
    private NodoParticipante ultimo;
    
    public ColaJugadores() {
        this.primero=null;
        this.ultimo=null;
    }
    
    public void agregar(String nom,int dinero,int casillasDesp,int peso,int numjug,int casillaVis, String col,boolean encarcelado,int cont,int numV){
        NodoParticipante temp = new NodoParticipante();
        temp.setInfo(nom, dinero, casillasDesp, peso, numjug, casillaVis, col, encarcelado, cont, numV);
        if(primero==null){
            primero = temp;
            ultimo = temp;
        }else{
            ultimo.setSiguiente(temp);
            ultimo = ultimo.getSiguiente();
            ultimo = temp;
        }
    }
    
    public String eliminar(){
        String mensaje="La lista est� vac�a";
        NodoParticipante temp;
        if(primero==null){
        } else{
            mensaje="\nEl elemento "+primero.getInfo().getNombre()+" ha sido eliminado ";
            temp = primero;
            primero = primero.getSiguiente();
            temp = null;
        }
        
        return mensaje;
    }
    public NodoParticipante devolverJugador(){
        
        NodoParticipante temp=null;
        if(primero==null){
        } else{
            
            temp = primero;
            
        }
        
        return temp;
    }
    
    public void moverJugador(){
        NodoParticipante temp=null;
        temp = devolverJugador();
        eliminar();
        agregar(temp.getInfo().getNombre(),temp.getInfo().getDinero(), temp.getInfo().getCasillasDesplazadas(),
                temp.getInfo().getPeso(),temp.getInfo().getNumJugador(), temp.getInfo().getCasillaVisitada(), temp.getInfo().getColor(),
                temp.getInfo().isEncarcelado(), temp.getInfo().getContador(), temp.getInfo().getNumeroV());
        
        
    }
    
    
    public String toString(){
        String mensaje= "";
        if(primero !=null)
            
            
            while(primero!=null)
                
                mensaje+=eliminar()+" ";
        
        
        
        return mensaje;
        
    }
    
    
    public NodoParticipante getPrimero() {
        return primero;
    }
    
    public void setPrimero(NodoParticipante primero) {
        this.primero = primero;
    }
    
    public NodoParticipante getUltimo() {
        return ultimo;
    }
    
    public void setUltimo(NodoParticipante ultimo) {
        this.ultimo = ultimo;
    }
}


