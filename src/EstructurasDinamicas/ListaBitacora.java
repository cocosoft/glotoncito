package EstructurasDinamicas;
/*Esta clase inserta la informaci�n de un participante y la despliega en pantalla*/

public class ListaBitacora {
    
    private NodoBitacora cabeza;
    
    public ListaBitacora() {
        this.cabeza = null;
    }
    
    public void InsertarInicio(String casVis, int peso){
        
        NodoBitacora temp = new NodoBitacora();
        temp.setInfo(casVis, peso);
        if(cabeza==null){
            cabeza = temp;
        }else{
            temp.setSiguiente(cabeza);
            cabeza = temp;
        }
    }
    public String Visualizar(){
        String Mensaje = "";
        NodoBitacora temp;
        if(cabeza==null){
            Mensaje = "La lista est� vac�a";
        }else{
            temp = cabeza;
            while(temp!=null){
                Mensaje = "La casillas visitadas: "+temp.getInfo().getCasillasVisitadas()+"\n"+"Peso Acumulado: "+String.valueOf(temp.getInfo().getPesoAcumulado());
                temp = temp.getSiguiente();
            }
        }
        return Mensaje;
    }
}
