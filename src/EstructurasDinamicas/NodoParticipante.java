package EstructurasDinamicas;
/*Esta clase tiene la información de un Participante y su respectiva referencia*/
import Jugador.*;


public class NodoParticipante {
    private Participante info;
    private NodoParticipante siguiente;
    
    public NodoParticipante() {
        this.info=null;
        this.setSiguiente(null);
    }
    
    public NodoParticipante(String nom,int dinero,int casillasDesp,int peso,int numjug,int casillaVis, String col,boolean encarcelado,int cont,int numV){
        this.info = new Participante(nom,dinero,casillasDesp,peso,numjug,casillaVis,col,encarcelado,cont, numV);   
    }
     public Participante getInfo() {
        return info;
    }

    public void setInfo(String nom,int dinero,int casillasDesp,int peso,int numjug,int casillaVis, String col,boolean encarcelado,int cont,int numV) {
        this.info= new Participante(nom,dinero,casillasDesp,peso,numjug,casillaVis,col,encarcelado, cont, numV);    
    }

    public NodoParticipante getSiguiente() {
        return siguiente;
    }

    public void setSiguiente(NodoParticipante siguiente) {
        this.siguiente = siguiente;
    }

    
    
}
