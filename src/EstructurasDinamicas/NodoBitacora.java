package EstructurasDinamicas;
/*Esta clase tiene la información de la bitacora y la almacena en un Nodo con su
 respectiva referencia*/
import Jugador.*;


public class NodoBitacora {
    
    private Bitacora info;
    private NodoBitacora siguiente;
    
    public NodoBitacora() {
        this.info = null;
        this.siguiente = null;
    }
    
    public NodoBitacora(String casVis, int peso) {
        this.info = new Bitacora(casVis, peso);
    }
    
    public Bitacora getInfo() {
        return info;
    }
    
    public void setInfo(String casVis, int peso) {
        this.info = new Bitacora(casVis, peso);
    }
    
    public NodoBitacora getSiguiente() {
        return siguiente;
    }
    
    public void setSiguiente(NodoBitacora siguiente) {
        this.siguiente = siguiente;
    }
    
}
