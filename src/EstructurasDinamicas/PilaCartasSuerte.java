package EstructurasDinamicas;
/**/
import Casilla.CartaSuerte;
import Casilla.Suerte;
import java.io.*;

public class PilaCartasSuerte {
    
    private NodoCartas cabeza;
    
    public PilaCartasSuerte() {
        cabeza = null;
    }
    public void Push(String mensage){
        NodoCartas temp = new NodoCartas();
        temp.setCartaSuerte(mensage);
        if(cabeza==null){
            cabeza=temp;
        }else{
            temp.setSiguiente(cabeza);
            cabeza=temp;
        }
        
    }
    
    
    public String PoP(){
        String dato="";
        if(cabeza == null){
        } else{
            NodoCartas temp = cabeza;
            dato=cabeza.getCartaSuerte().getMensage();
            cabeza = cabeza.getSiguiente();
            temp.setSiguiente(null);
            temp = null;
        }
        return dato;
    }
    public void guardar(String nomArchivo){
        ObjectOutputStream salida = null;
        try{
            salida = new ObjectOutputStream(new FileOutputStream(nomArchivo)); 
        }catch(IOException e){
            System.err.println("Error de apertura");
        }
        NodoCartas temp = cabeza;
        
        while(temp!=null){
            try{
                salida.writeObject(temp.getCartaSuerte());
            }catch(IOException f){
                System.err.println("Error al guardar");
            }
            temp = temp.getSiguiente(); 
        }
        try{
            salida.close();
        }catch(IOException g){
            System.err.println("Error al cerrar archivo");
        }
    }
    public void CargarArchivo(String nomArchivo){
      ObjectInputStream entrada = null;
      try{
          entrada = new ObjectInputStream(new FileInputStream(new File(nomArchivo))); 
      }catch(IOException e){
          System.err.println("Error de apertura");
      }
      NodoCartas temp = cabeza;
      CartaSuerte dato = null;
      do{
          try{
              dato = (CartaSuerte)entrada.readObject();
              this.Push(dato.getMensage()); 
          }catch(EOFException f){
              dato = null;
          }catch(IOException g){
              System.err.println("Error al cargar elemento");
          }catch(ClassNotFoundException h){
              System.err.println("No esta clase elemento");
          }
          try{
           temp = temp.getSiguiente();
          }catch(NullPointerException f){
              //System.out.println("Final de la la p�la");
          }
          }while(dato!=null);
      }
    
    
    
    public NodoCartas getCabeza() {
        return cabeza;
    }
    
    public void setCabeza(NodoCartas cabeza) {
        this.cabeza = cabeza;
    }
    
}
