package EstructurasDinamicas;
/*Esta clase tiene la información de la carta de la suerte y su respectiva 
 referencia*/
import Casilla.*;

public class NodoCartas {
    
    private CartaSuerte cartaSuerte;
    private NodoCartas siguiente;
    
    public NodoCartas() {
        this.cartaSuerte=null;
        this.setSiguiente(null);
    }
    public NodoCartas(String mensage){
        this.cartaSuerte = new CartaSuerte(mensage);
    }
    
    public CartaSuerte getCartaSuerte() {
        return cartaSuerte;
    }
    
    public void setCartaSuerte(String mensage) {
        this.cartaSuerte = new CartaSuerte(mensage);
    }
    
    public NodoCartas getSiguiente() {
        return siguiente;
    }
    
    public void setSiguiente(NodoCartas siguiente) {
        this.siguiente = siguiente;
    }
    
}
