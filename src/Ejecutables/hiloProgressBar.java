/*En esta clase se crea la barra de progreso
 que se utiliza en la clase Bienvenido*/
package Ejecutables;
import javax.swing.JProgressBar;

public class hiloProgressBar extends Thread{
    
    private javax.swing.JProgressBar jProgressBarInicio;
    
    public hiloProgressBar() {
    }
    
    public void run(){
        /*Este m�todo recorre la barra de progreso*/
        try{
            for(int i = jProgressBarInicio.getMinimum(); i <= jProgressBarInicio.getMaximum(); i++){
                jProgressBarInicio.setValue(i);
                sleep(30);//duraci�n de la barra
            }
        }catch(InterruptedException e){;}
        
    }
    
    public javax.swing.JProgressBar getJProgressBarInicio() {
        return jProgressBarInicio;
    }
    
    public void setJProgressBarInicio(javax.swing.JProgressBar jProgressBarInicio) {
        this.jProgressBarInicio = jProgressBarInicio;
    }
    
}
