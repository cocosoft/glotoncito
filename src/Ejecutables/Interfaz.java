package Ejecutables;
/*Esta es la clase interfaz donde se implementan todos los m�todos para mover 
 al jugador en el tablero*/
import Casilla.*;
import java.awt.*;
import Juego.*;
import Jugador.*;
import javax.swing.*;
import EstructurasDinamicas.*;

public class Interfaz extends javax.swing.JFrame {
    
    NodoParticipante np;
    Tablero j;
    ColaJugadores cj;
    private boolean banderola;
    private boolean band;
    private boolean inicio1;
    private boolean inicio2;
    private boolean inicio3;
    private boolean inicio4;
    private boolean inicio5;
    private boolean inicio6;
    private boolean inicio7;
    private boolean inicio8;
    private int contGlobal;
    
    
    public Interfaz() {
        initComponents();
        labelJuego.setVisible(true);
        this.banderola = false;
        this.band = false;
        cj = new ColaJugadores();
        np = new NodoParticipante();
        setLocation(50,70);
        j = new Tablero();
        jLabel70.setVisible(false);
        Dado1.setVisible(false);
        Dado2.setVisible(false);
        Dado3.setVisible(false);
        Dado4.setVisible(false);
        Dado5.setVisible(false);
        Dado6.setVisible(false);
        BotonBitacora.setEnabled(false);
        Jug1Turno.setVisible(false);
        Jug2Turno.setVisible(false);
        Jug3Turno.setVisible(false);
        Jug4Turno.setVisible(false);
        NomJug1.setEnabled(false);
        NomJug2.setEnabled(false);
        NomJug3.setEnabled(false);
        NomJug4.setEnabled(false);
        Iniciar.setEnabled(false);
        PanelInfo.setVisible(false);
        PanelTablero.setVisible(false);
        labelGanador1.setVisible(false);
        labelGanador2.setVisible(false);
        labelGanador3.setVisible(false);
        labelGanador4.setVisible(false);
        j.CrearTablero();
        j.llenarPilaSuerte();
        inicio1=false;
        inicio2=false;
        inicio3=false;
        inicio4=false;
        inicio5=false;
        inicio6=false;
        inicio7=false;
        inicio8=false;
        contGlobal=0;
        j.cargarRecord();
    }
    
    
    // <editor-fold defaultstate="collapsed" desc=" Generated Code ">//GEN-BEGIN:initComponents
    private void initComponents() {
        jPopupMenu1 = new javax.swing.JPopupMenu();
        salir = new javax.swing.JMenuItem();
        mensaje = new javax.swing.JMenuItem();
        PanelTablero = new javax.swing.JPanel();
        Casilla16 = new javax.swing.JPanel();
        jLabel49 = new javax.swing.JLabel();
        jLabel29 = new javax.swing.JLabel();
        Casilla15 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        Casilla18 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        Casilla17 = new javax.swing.JPanel();
        jLabel50 = new javax.swing.JLabel();
        jLabel30 = new javax.swing.JLabel();
        Casilla22 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        Casilla21 = new javax.swing.JPanel();
        jLabel54 = new javax.swing.JLabel();
        jLabel33 = new javax.swing.JLabel();
        Casilla20 = new javax.swing.JPanel();
        jLabel52 = new javax.swing.JLabel();
        jLabel32 = new javax.swing.JLabel();
        Casilla19 = new javax.swing.JPanel();
        jLabel51 = new javax.swing.JLabel();
        jLabel31 = new javax.swing.JLabel();
        Casilla1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        Jugador1 = new javax.swing.JLabel();
        Jugador2 = new javax.swing.JLabel();
        Jugador3 = new javax.swing.JLabel();
        Jugador4 = new javax.swing.JLabel();
        Casilla2 = new javax.swing.JPanel();
        jLabel39 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        Casilla3 = new javax.swing.JPanel();
        jLabel41 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        Casilla4 = new javax.swing.JPanel();
        jLabel42 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        Casilla5 = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        Casilla6 = new javax.swing.JPanel();
        jLabel43 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        Casilla7 = new javax.swing.JPanel();
        jLabel40 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        Casilla8 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        Casilla27 = new javax.swing.JPanel();
        jLabel57 = new javax.swing.JLabel();
        jLabel37 = new javax.swing.JLabel();
        Casilla25 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        Casilla26 = new javax.swing.JPanel();
        jLabel56 = new javax.swing.JLabel();
        jLabel36 = new javax.swing.JLabel();
        Casilla23 = new javax.swing.JPanel();
        jLabel55 = new javax.swing.JLabel();
        jLabel34 = new javax.swing.JLabel();
        Casilla24 = new javax.swing.JPanel();
        jLabel53 = new javax.swing.JLabel();
        jLabel35 = new javax.swing.JLabel();
        Casilla28 = new javax.swing.JPanel();
        jLabel58 = new javax.swing.JLabel();
        jLabel38 = new javax.swing.JLabel();
        Casilla9 = new javax.swing.JPanel();
        jLabel44 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        Casilla10 = new javax.swing.JPanel();
        jLabel45 = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        Casilla11 = new javax.swing.JPanel();
        jLabel48 = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        Casilla12 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        Casilla13 = new javax.swing.JPanel();
        jLabel47 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        Casilla14 = new javax.swing.JPanel();
        jLabel46 = new javax.swing.JLabel();
        jLabel28 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel70 = new javax.swing.JLabel();
        labelGanador1 = new javax.swing.JLabel();
        labelGanador2 = new javax.swing.JLabel();
        labelGanador3 = new javax.swing.JLabel();
        labelGanador4 = new javax.swing.JLabel();
        labelJuego = new javax.swing.JLabel();
        PanelInfo = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        lanzarDado = new javax.swing.JButton();
        jPanel33 = new javax.swing.JPanel();
        Dado1 = new javax.swing.JLabel();
        Dado2 = new javax.swing.JLabel();
        Dado3 = new javax.swing.JLabel();
        Dado4 = new javax.swing.JLabel();
        Dado5 = new javax.swing.JLabel();
        Dado6 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jButton1 = new javax.swing.JButton();
        jSeparator2 = new javax.swing.JSeparator();
        jButton2 = new javax.swing.JButton();
        jSeparator3 = new javax.swing.JSeparator();
        BotonBitacora = new javax.swing.JButton();
        jPanel32 = new javax.swing.JPanel();
        Comprar = new javax.swing.JButton();
        TurnoSiguiente = new javax.swing.JButton();
        infoNom = new javax.swing.JTextField();
        infoDinero = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        infoPeso = new javax.swing.JTextField();
        infoVueltas = new javax.swing.JTextField();
        infoColor = new javax.swing.JTextField();
        infoNum = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        infoCasillaActual = new javax.swing.JTextField();
        jPanel1 = new javax.swing.JPanel();
        jLabel69 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        Jug1Turno = new javax.swing.JLabel();
        Jug2Turno = new javax.swing.JLabel();
        Jug3Turno = new javax.swing.JLabel();
        Jug4Turno = new javax.swing.JLabel();
        PanelPrincipal = new javax.swing.JPanel();
        jLabel59 = new javax.swing.JLabel();
        jLabel60 = new javax.swing.JLabel();
        RadioJug1 = new javax.swing.JRadioButton();
        RadioJug2 = new javax.swing.JRadioButton();
        RadioJug3 = new javax.swing.JRadioButton();
        RadioJug4 = new javax.swing.JRadioButton();
        jLabel61 = new javax.swing.JLabel();
        jLabel62 = new javax.swing.JLabel();
        jLabel63 = new javax.swing.JLabel();
        NomJug1 = new javax.swing.JTextField();
        NomJug2 = new javax.swing.JTextField();
        jLabel64 = new javax.swing.JLabel();
        NomJug3 = new javax.swing.JTextField();
        jLabel65 = new javax.swing.JLabel();
        NomJug4 = new javax.swing.JTextField();
        jLabel66 = new javax.swing.JLabel();
        jLabel67 = new javax.swing.JLabel();
        jLabel68 = new javax.swing.JLabel();
        Iniciar = new javax.swing.JButton();

        salir.setText("Salir");
        salir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                salirActionPerformed(evt);
            }
        });

        jPopupMenu1.add(salir);

        mensaje.setText("Mensaje");
        mensaje.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mensajeActionPerformed(evt);
            }
        });

        jPopupMenu1.add(mensaje);

        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                formKeyReleased(evt);
            }
        });

        PanelTablero.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        PanelTablero.setBackground(new java.awt.Color(255, 255, 255));
        PanelTablero.setBorder(new javax.swing.border.EtchedBorder());
        PanelTablero.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                PanelTableroKeyReleased(evt);
            }
        });
        PanelTablero.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                PanelTableroMouseClicked(evt);
            }
        });

        Casilla16.setBackground(new java.awt.Color(255, 255, 255));
        Casilla16.setBorder(new javax.swing.border.EtchedBorder());
        jLabel49.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel49.setText(" Pizza Hut  ");
        Casilla16.add(jLabel49);

        jLabel29.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel29.setText("        $500       ");
        Casilla16.add(jLabel29);

        PanelTablero.add(Casilla16, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 0, 90, 80));

        Casilla15.setBackground(new java.awt.Color(0, 51, 102));
        Casilla15.setBorder(new javax.swing.border.EtchedBorder());
        jLabel4.setFont(new java.awt.Font("Eras Demi ITC", 0, 12));
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel4.setText("      De Paso     ");
        Casilla15.add(jLabel4);

        PanelTablero.add(Casilla15, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 90, 80));

        Casilla18.setBackground(new java.awt.Color(51, 51, 51));
        Casilla18.setBorder(new javax.swing.border.EtchedBorder());
        jLabel9.setFont(new java.awt.Font("Eras Demi ITC", 0, 12));
        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel9.setText("     Castigo     ");
        Casilla18.add(jLabel9);

        PanelTablero.add(Casilla18, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 0, 90, 80));

        Casilla17.setBackground(new java.awt.Color(255, 255, 255));
        Casilla17.setBorder(new javax.swing.border.EtchedBorder());
        jLabel50.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel50.setText("      KFC    ");
        Casilla17.add(jLabel50);

        jLabel30.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel30.setText("        $500       ");
        Casilla17.add(jLabel30);

        PanelTablero.add(Casilla17, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 0, 90, 80));

        Casilla22.setBackground(new java.awt.Color(204, 0, 0));
        Casilla22.setBorder(new javax.swing.border.EtchedBorder());
        jLabel5.setFont(new java.awt.Font("Eras Demi ITC", 0, 12));
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel5.setText("   Ir a Carcel    ");
        Casilla22.add(jLabel5);

        PanelTablero.add(Casilla22, new org.netbeans.lib.awtextra.AbsoluteConstraints(630, 0, 90, 80));

        Casilla21.setBackground(new java.awt.Color(255, 255, 255));
        Casilla21.setBorder(new javax.swing.border.EtchedBorder());
        jLabel54.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel54.setText("El Fogoncito");
        Casilla21.add(jLabel54);

        jLabel33.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel33.setText("        $600      ");
        Casilla21.add(jLabel33);

        PanelTablero.add(Casilla21, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 0, 90, 80));

        Casilla20.setBackground(new java.awt.Color(255, 255, 255));
        Casilla20.setBorder(new javax.swing.border.EtchedBorder());
        jLabel52.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel52.setText("     Subway      ");
        Casilla20.add(jLabel52);

        jLabel32.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel32.setText("       $600        ");
        Casilla20.add(jLabel32);

        PanelTablero.add(Casilla20, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 0, 90, 80));

        Casilla19.setBackground(new java.awt.Color(255, 255, 255));
        Casilla19.setBorder(new javax.swing.border.EtchedBorder());
        jLabel51.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel51.setText("    Taco Bell     ");
        Casilla19.add(jLabel51);

        jLabel31.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel31.setText("        $600       ");
        Casilla19.add(jLabel31);

        PanelTablero.add(Casilla19, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 0, 90, 80));

        Casilla1.setBackground(new java.awt.Color(0, 51, 102));
        Casilla1.setBorder(new javax.swing.border.EtchedBorder());
        jLabel1.setFont(new java.awt.Font("Eras Demi ITC", 0, 12));
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("    INICIO    ");
        Casilla1.add(jLabel1);

        Jugador1.setForeground(new java.awt.Color(255, 0, 0));
        Jugador1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cabeza1.jpg")));
        Casilla1.add(Jugador1);

        Jugador2.setForeground(new java.awt.Color(255, 0, 0));
        Jugador2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cabeza2.jpg")));
        Casilla1.add(Jugador2);

        Jugador3.setForeground(new java.awt.Color(255, 0, 0));
        Jugador3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cabeza3.jpg")));
        Casilla1.add(Jugador3);

        Jugador4.setForeground(new java.awt.Color(255, 0, 0));
        Jugador4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cabeza4.jpg")));
        Casilla1.add(Jugador4);

        PanelTablero.add(Casilla1, new org.netbeans.lib.awtextra.AbsoluteConstraints(630, 560, 90, 80));

        Casilla2.setBackground(new java.awt.Color(255, 255, 255));
        Casilla2.setBorder(new javax.swing.border.EtchedBorder());
        jLabel39.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel39.setText("Candy Shop");
        Casilla2.add(jLabel39);

        jLabel20.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel20.setText("      $100      ");
        Casilla2.add(jLabel20);

        PanelTablero.add(Casilla2, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 560, 90, 80));

        Casilla3.setBackground(new java.awt.Color(255, 255, 255));
        Casilla3.setBorder(new javax.swing.border.EtchedBorder());
        jLabel41.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel41.setText("   Vishnu   ");
        Casilla3.add(jLabel41);

        jLabel21.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel21.setText("         $100      ");
        Casilla3.add(jLabel21);

        PanelTablero.add(Casilla3, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 560, 90, 80));

        Casilla4.setBackground(new java.awt.Color(255, 255, 255));
        Casilla4.setBorder(new javax.swing.border.EtchedBorder());
        jLabel42.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel42.setText("  Soda la U ");
        Casilla4.add(jLabel42);

        jLabel22.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel22.setText("      $100        ");
        Casilla4.add(jLabel22);

        PanelTablero.add(Casilla4, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 560, 90, 80));

        Casilla5.setBackground(new java.awt.Color(51, 51, 51));
        Casilla5.setBorder(new javax.swing.border.EtchedBorder());
        jLabel10.setFont(new java.awt.Font("Eras Demi ITC", 0, 12));
        jLabel10.setForeground(new java.awt.Color(255, 255, 255));
        jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel10.setText("    Castigo    ");
        Casilla5.add(jLabel10);

        PanelTablero.add(Casilla5, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 560, 90, 80));

        Casilla6.setBackground(new java.awt.Color(255, 255, 255));
        Casilla6.setBorder(new javax.swing.border.EtchedBorder());
        jLabel43.setFont(new java.awt.Font("Tahoma", 0, 10));
        jLabel43.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel43.setText("Pollo Sus Amigos");
        Casilla6.add(jLabel43);

        jLabel11.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel11.setText("       $200     ");
        Casilla6.add(jLabel11);

        PanelTablero.add(Casilla6, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 560, 90, 80));

        Casilla7.setBackground(new java.awt.Color(255, 255, 255));
        Casilla7.setBorder(new javax.swing.border.EtchedBorder());
        jLabel40.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel40.setText("Pizzer\u00eda Latina");
        Casilla7.add(jLabel40);

        jLabel23.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel23.setText("      $200     ");
        Casilla7.add(jLabel23);

        PanelTablero.add(Casilla7, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 560, 90, 80));

        Casilla8.setBackground(new java.awt.Color(204, 0, 0));
        Casilla8.setBorder(new javax.swing.border.EtchedBorder());
        jLabel3.setFont(new java.awt.Font("Eras Demi ITC", 0, 12));
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText("    CARCEL    ");
        Casilla8.add(jLabel3);

        PanelTablero.add(Casilla8, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 560, 90, 80));

        Casilla27.setBackground(new java.awt.Color(255, 255, 255));
        Casilla27.setBorder(new javax.swing.border.EtchedBorder());
        jLabel57.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel57.setText("Il Pomodoro");
        Casilla27.add(jLabel57);

        jLabel37.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel37.setText("       $800       ");
        Casilla27.add(jLabel37);

        PanelTablero.add(Casilla27, new org.netbeans.lib.awtextra.AbsoluteConstraints(630, 400, 90, 80));

        Casilla25.setBackground(new java.awt.Color(37, 125, 7));
        Casilla25.setBorder(new javax.swing.border.EtchedBorder());
        jLabel8.setFont(new java.awt.Font("Eras Demi ITC", 0, 12));
        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel8.setText("      Suerte     ");
        Casilla25.add(jLabel8);

        PanelTablero.add(Casilla25, new org.netbeans.lib.awtextra.AbsoluteConstraints(630, 240, 90, 80));

        Casilla26.setBackground(new java.awt.Color(255, 255, 255));
        Casilla26.setBorder(new javax.swing.border.EtchedBorder());
        jLabel56.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel56.setText("  Friday\u00b4s ");
        Casilla26.add(jLabel56);

        jLabel36.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel36.setText("        $800      ");
        Casilla26.add(jLabel36);

        PanelTablero.add(Casilla26, new org.netbeans.lib.awtextra.AbsoluteConstraints(630, 320, 90, 80));

        Casilla23.setBackground(new java.awt.Color(255, 255, 255));
        Casilla23.setBorder(new javax.swing.border.EtchedBorder());
        jLabel55.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel55.setText("RostiPollos");
        Casilla23.add(jLabel55);

        jLabel34.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel34.setText("       $700       ");
        Casilla23.add(jLabel34);

        PanelTablero.add(Casilla23, new org.netbeans.lib.awtextra.AbsoluteConstraints(630, 80, 90, 80));

        Casilla24.setBackground(new java.awt.Color(255, 255, 255));
        Casilla24.setBorder(new javax.swing.border.EtchedBorder());
        jLabel53.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel53.setText("    Il Gato  ");
        Casilla24.add(jLabel53);

        jLabel35.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel35.setText("       $700       ");
        Casilla24.add(jLabel35);

        PanelTablero.add(Casilla24, new org.netbeans.lib.awtextra.AbsoluteConstraints(630, 160, 90, 80));

        Casilla28.setBackground(new java.awt.Color(255, 255, 255));
        Casilla28.setBorder(new javax.swing.border.EtchedBorder());
        jLabel58.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel58.setText("      Denny\u00b4s     ");
        Casilla28.add(jLabel58);

        jLabel38.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel38.setText("       $800        ");
        Casilla28.add(jLabel38);

        PanelTablero.add(Casilla28, new org.netbeans.lib.awtextra.AbsoluteConstraints(630, 480, 90, 80));

        Casilla9.setBackground(new java.awt.Color(255, 255, 255));
        Casilla9.setBorder(new javax.swing.border.EtchedBorder());
        jLabel44.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel44.setText("   Tico Burguesas  ");
        Casilla9.add(jLabel44);

        jLabel24.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel24.setText("      $300    ");
        Casilla9.add(jLabel24);

        PanelTablero.add(Casilla9, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 480, 90, 80));

        Casilla10.setBackground(new java.awt.Color(255, 255, 255));
        Casilla10.setBorder(new javax.swing.border.EtchedBorder());
        jLabel45.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel45.setText("2 x 1 Pizza");
        Casilla10.add(jLabel45);

        jLabel25.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel25.setText("      $300       ");
        Casilla10.add(jLabel25);

        PanelTablero.add(Casilla10, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 400, 90, 80));

        Casilla11.setBackground(new java.awt.Color(255, 255, 255));
        Casilla11.setBorder(new javax.swing.border.EtchedBorder());
        jLabel48.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel48.setText("     As de Oros    ");
        Casilla11.add(jLabel48);

        jLabel26.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel26.setText("        $300      ");
        Casilla11.add(jLabel26);

        PanelTablero.add(Casilla11, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 320, 90, 80));

        Casilla12.setBackground(new java.awt.Color(37, 125, 7));
        Casilla12.setBorder(new javax.swing.border.EtchedBorder());
        jLabel7.setFont(new java.awt.Font("Eras Demi ITC", 0, 12));
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel7.setText("     Suerte    ");
        Casilla12.add(jLabel7);

        PanelTablero.add(Casilla12, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 240, 90, 80));

        Casilla13.setBackground(new java.awt.Color(255, 255, 255));
        Casilla13.setBorder(new javax.swing.border.EtchedBorder());
        jLabel47.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel47.setText("Burger King");
        Casilla13.add(jLabel47);

        jLabel27.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel27.setText("       $400      ");
        Casilla13.add(jLabel27);

        PanelTablero.add(Casilla13, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 160, 90, 80));

        Casilla14.setBackground(new java.awt.Color(255, 255, 255));
        Casilla14.setBorder(new javax.swing.border.EtchedBorder());
        jLabel46.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel46.setText("McDonalds");
        Casilla14.add(jLabel46);

        jLabel28.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel28.setText("       $400        ");
        Casilla14.add(jLabel28);

        PanelTablero.add(Casilla14, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 80, 90, 80));

        jLabel2.setText("INICIO");
        PanelTablero.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 30, -1, -1));

        jLabel6.setFont(new java.awt.Font("Eras Demi ITC", 0, 12));
        jLabel6.setText("De Paso");
        PanelTablero.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 10, -1, -1));

        jLabel70.setFont(new java.awt.Font("Tahoma", 1, 30));
        jLabel70.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        PanelTablero.add(jLabel70, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 440, 380, 30));

        labelGanador1.setBackground(new java.awt.Color(255, 255, 255));
        labelGanador1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelGanador1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/ganador1.jpg")));
        PanelTablero.add(labelGanador1, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 80, 540, 480));

        labelGanador2.setBackground(new java.awt.Color(255, 255, 255));
        labelGanador2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelGanador2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/ganador2.jpg")));
        PanelTablero.add(labelGanador2, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 80, 540, 480));

        labelGanador3.setBackground(new java.awt.Color(255, 255, 255));
        labelGanador3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelGanador3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/ganador3.jpg")));
        PanelTablero.add(labelGanador3, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 80, 540, 480));

        labelGanador4.setBackground(new java.awt.Color(255, 255, 255));
        labelGanador4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelGanador4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/ganador4.jpg")));
        PanelTablero.add(labelGanador4, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 80, 540, 480));

        labelJuego.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/imagenJuego.jpg")));
        PanelTablero.add(labelJuego, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 80, 540, 480));

        getContentPane().add(PanelTablero, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 720, 640));

        PanelInfo.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        PanelInfo.setBorder(new javax.swing.border.EtchedBorder());
        jPanel3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel3.setBorder(new javax.swing.border.BevelBorder(javax.swing.border.BevelBorder.RAISED));
        lanzarDado.setMnemonic('L');
        lanzarDado.setText("Lanzar Dado");
        lanzarDado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                lanzarDadoActionPerformed(evt);
            }
        });

        jPanel3.add(lanzarDado, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 130, 110, -1));

        jPanel33.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel33.setBorder(new javax.swing.border.BevelBorder(javax.swing.border.BevelBorder.LOWERED));
        Dado1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Dado1.JPG")));
        jPanel33.add(Dado1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, -1, -1));

        Dado2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Dado2.JPG")));
        jPanel33.add(Dado2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, -1, -1));

        Dado3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Dado3.JPG")));
        jPanel33.add(Dado3, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, -1, -1));

        Dado4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Dado4.JPG")));
        jPanel33.add(Dado4, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, -1, -1));

        Dado5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Dado5.JPG")));
        jPanel33.add(Dado5, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, -1, -1));

        Dado6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Dado6.JPG")));
        jPanel33.add(Dado6, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, -1, -1));

        jPanel3.add(jPanel33, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 10, 110, 110));

        jSeparator1.setOrientation(javax.swing.SwingConstants.VERTICAL);
        jPanel3.add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 0, 10, 160));

        jButton1.setFont(new java.awt.Font("Tahoma", 1, 12));
        jButton1.setMnemonic('a');
        jButton1.setText("Ver Ayuda");
        jButton1.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jPanel3.add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 10, 100, 30));

        jPanel3.add(jSeparator2, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 50, 140, 10));

        jButton2.setFont(new java.awt.Font("Tahoma", 1, 12));
        jButton2.setMnemonic('r');
        jButton2.setText("Ver Records");
        jButton2.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jPanel3.add(jButton2, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 60, 100, 30));

        jPanel3.add(jSeparator3, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 100, 140, 10));

        BotonBitacora.setFont(new java.awt.Font("Tahoma", 1, 12));
        BotonBitacora.setMnemonic('a');
        BotonBitacora.setText("Ver Bitacora");
        BotonBitacora.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        BotonBitacora.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotonBitacoraActionPerformed(evt);
            }
        });

        jPanel3.add(BotonBitacora, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 110, 100, 30));

        PanelInfo.add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 470, 300, 160));

        jPanel32.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel32.setBorder(new javax.swing.border.BevelBorder(javax.swing.border.BevelBorder.RAISED));
        Comprar.setMnemonic('C');
        Comprar.setText("Comprar");
        Comprar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ComprarActionPerformed(evt);
            }
        });

        jPanel32.add(Comprar, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 200, 110, -1));

        TurnoSiguiente.setMnemonic('T');
        TurnoSiguiente.setText("Turno Siguiente");
        TurnoSiguiente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TurnoSiguienteActionPerformed(evt);
            }
        });

        jPanel32.add(TurnoSiguiente, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 200, -1, -1));

        infoNom.setEditable(false);
        jPanel32.add(infoNom, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 50, 90, -1));

        infoDinero.setEditable(false);
        jPanel32.add(infoDinero, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 70, 90, -1));

        jLabel12.setFont(new java.awt.Font("Tahoma", 1, 13));
        jLabel12.setText("INFORMACION DEL JUGADOR EN TURNO");
        jPanel32.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 10, -1, -1));

        infoPeso.setEditable(false);
        jPanel32.add(infoPeso, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 90, 90, -1));

        infoVueltas.setEditable(false);
        jPanel32.add(infoVueltas, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 110, 90, -1));

        infoColor.setEditable(false);
        jPanel32.add(infoColor, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 130, 90, -1));

        infoNum.setEditable(false);
        jPanel32.add(infoNum, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 150, 90, -1));

        jLabel13.setText("Nombre:");
        jPanel32.add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 50, -1, -1));

        jLabel14.setText("Dinero Disponible:");
        jPanel32.add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 70, -1, -1));

        jLabel15.setText("Peso:");
        jPanel32.add(jLabel15, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 90, -1, -1));

        jLabel16.setText("Num. de Vueltas:");
        jPanel32.add(jLabel16, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 110, -1, -1));

        jLabel17.setText("Color:");
        jPanel32.add(jLabel17, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 130, -1, -1));

        jLabel18.setText("Numero Jugador:");
        jPanel32.add(jLabel18, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 150, -1, -1));

        jLabel19.setText("Casilla Actual:");
        jPanel32.add(jLabel19, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 170, -1, -1));

        infoCasillaActual.setEditable(false);
        jPanel32.add(infoCasillaActual, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 170, 90, -1));

        PanelInfo.add(jPanel32, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 300, 240));

        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBorder(new javax.swing.border.BevelBorder(javax.swing.border.BevelBorder.RAISED));
        jLabel69.setFont(new java.awt.Font("Tahoma", 1, 13));
        jLabel69.setText("JUGADOR EN TURNO");
        jPanel1.add(jLabel69, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 10, -1, -1));

        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel2.setBorder(new javax.swing.border.BevelBorder(javax.swing.border.BevelBorder.LOWERED));
        Jug1Turno.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Jug1.JPG")));
        jPanel2.add(Jug1Turno, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 10, -1, 160));

        Jug2Turno.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Jug2.JPG")));
        jPanel2.add(Jug2Turno, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 10, -1, 160));

        Jug3Turno.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Juga3.jpg")));
        jPanel2.add(Jug3Turno, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 10, -1, 160));

        Jug4Turno.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Juga4.JPG")));
        jPanel2.add(Jug4Turno, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 10, -1, -1));

        jPanel1.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 30, 200, 180));

        PanelInfo.add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 250, 300, 220));

        getContentPane().add(PanelInfo, new org.netbeans.lib.awtextra.AbsoluteConstraints(720, 0, 320, 640));

        PanelPrincipal.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel59.setFont(new java.awt.Font("Tahoma", 1, 48));
        jLabel59.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel59.setText("EL GLOTONCITO");
        jLabel59.setBorder(new javax.swing.border.EtchedBorder());
        PanelPrincipal.add(jLabel59, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 20, 560, 120));

        jLabel60.setFont(new java.awt.Font("Tahoma", 1, 14));
        jLabel60.setText("SELECCIONE JUGADOR");
        PanelPrincipal.add(jLabel60, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 170, 170, 30));

        RadioJug1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RadioJug1ActionPerformed(evt);
            }
        });

        PanelPrincipal.add(RadioJug1, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 230, -1, -1));

        RadioJug2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RadioJug2ActionPerformed(evt);
            }
        });

        PanelPrincipal.add(RadioJug2, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 230, -1, -1));

        RadioJug3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RadioJug3ActionPerformed(evt);
            }
        });

        PanelPrincipal.add(RadioJug3, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 230, -1, -1));

        RadioJug4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RadioJug4ActionPerformed(evt);
            }
        });

        PanelPrincipal.add(RadioJug4, new org.netbeans.lib.awtextra.AbsoluteConstraints(740, 230, -1, -1));

        jLabel61.setFont(new java.awt.Font("Tahoma", 1, 14));
        jLabel61.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Jug1.JPG")));
        PanelPrincipal.add(jLabel61, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 270, 140, 170));

        jLabel62.setFont(new java.awt.Font("Tahoma", 1, 14));
        jLabel62.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Jug2.JPG")));
        PanelPrincipal.add(jLabel62, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 270, 150, 170));

        jLabel63.setText("Nombre:");
        PanelPrincipal.add(jLabel63, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 470, 50, 20));

        PanelPrincipal.add(NomJug1, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 470, 80, -1));

        PanelPrincipal.add(NomJug2, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 470, 80, -1));

        jLabel64.setText("Nombre:");
        PanelPrincipal.add(jLabel64, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 470, 50, 20));

        PanelPrincipal.add(NomJug3, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 470, 80, -1));

        jLabel65.setText("Nombre:");
        PanelPrincipal.add(jLabel65, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 470, 50, 20));

        PanelPrincipal.add(NomJug4, new org.netbeans.lib.awtextra.AbsoluteConstraints(740, 470, 80, -1));

        jLabel66.setText("Nombre:");
        PanelPrincipal.add(jLabel66, new org.netbeans.lib.awtextra.AbsoluteConstraints(690, 460, 50, 20));

        jLabel67.setFont(new java.awt.Font("Tahoma", 1, 14));
        jLabel67.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Juga3.JPG")));
        PanelPrincipal.add(jLabel67, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 270, -1, 170));

        jLabel68.setFont(new java.awt.Font("Tahoma", 1, 14));
        jLabel68.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Juga4.JPG")));
        PanelPrincipal.add(jLabel68, new org.netbeans.lib.awtextra.AbsoluteConstraints(680, 270, 150, 170));

        Iniciar.setFont(new java.awt.Font("Tahoma", 1, 12));
        Iniciar.setText("INICIAR JUEGO");
        Iniciar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                IniciarActionPerformed(evt);
            }
        });

        PanelPrincipal.add(Iniciar, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 550, 180, 40));

        getContentPane().add(PanelPrincipal, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1040, 640));

        pack();
    }
    // </editor-fold>//GEN-END:initComponents

    private void BotonBitacoraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonBitacoraActionPerformed
        JOptionPane.showMessageDialog(null,np.getInfo().getListaBitacora().Visualizar());
    }//GEN-LAST:event_BotonBitacoraActionPerformed
    
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        /*Este m�todo le muestra al usuario una ventana de ayuda para que se 
         gu�e durante el juego*/
        JTextArea a = new JTextArea();
        a.append(" ****** AYUDA DEL GLOTONCITO ********\n\n REGLAS GENERALES\n\n1. Todos los jugadores inician el juego con un monto de 10,000 colones"+
                "\n 2. Para jugar, cada persona simplemente debe lanzar el dado (presionar el bot�n LANZAR DADOy esperar el resultado de su suerte"+
                "\n 3. Cada vez que un jugador cae en una casilla de tipo Restaurante, tiene derecho a decidir si quiere o no comprar lo que le venden en esa tienda"+
                "\n 4. Si decide comprar solamente debe presional el boton COMPRAR (siempre y cuando tenga dinero) de lo contrario presiona el boton TURNO SIGUIENTE  "+
                "\n 5.Si el jugador cae en una casilla CASTIGO, autom�ticamente se devolver� 2 espacios"+
                "\n 6. Si el jugador cae en la casilla IR A CARCEL, autom�ticamente ser� enviado hasta la CARCEL y tiene derecho a lanzar el dado."+
                "\n Si lanza un n�mero mayor a 3, tiene derecho a salir inmediatamente, de lo contrario debe esperar un turno"+
                "\n 7. Si el jugador cae en una casilla del tipo SUERTE, podr�a tener o no tener suerte mediante lo que indique la carta que saca autom�ticamente"+
                "\n Es decir, podr�a recibir solamente un mensaje, perder peso, ganar peso, perder dinero, ganar dinero, etc"+
                "\n 8. Cada vez que un jugador pasa por la casilla INICIO, recibe un premio de 2,000 colones"+
                "\n 9. El primer jugador en completar 3 vueltas es autom�ticamente declarado como el ganador e ingresa a la lista de RECORDS");
        JOptionPane.showMessageDialog(null,a,"AYUDA",JOptionPane.INFORMATION_MESSAGE);
        
    }//GEN-LAST:event_jButton1ActionPerformed
    
    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        /*Este m�todo despliega el peso y el nombre de los mejores jugadores*/
        j.desplegarRecord();
        
    }//GEN-LAST:event_jButton2ActionPerformed
    
    private void TurnoSiguienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TurnoSiguienteActionPerformed
        /*Este m�todo hace que siga el siguiente jugador en la cola de jugadores*/
        cj.moverJugador();
        Comprar.setEnabled(false);
        banderola = false;
        band = false;
        TurnoSiguiente.setEnabled(false);
        lanzarDado.setEnabled(true);
    }//GEN-LAST:event_TurnoSiguienteActionPerformed
    
    private void ValidarBoton(){
        /*Este m�todo valida que todos los jugadores est�n seleccionados*/
        if((RadioJug1.isSelected()==true)&&(RadioJug2.isSelected()==true)&&(RadioJug3.isSelected()==true)&&(RadioJug4.isSelected()==true)){
            Iniciar.setEnabled(true);
        }else
            Iniciar.setEnabled(false);
        
    }
    private void IniciarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_IniciarActionPerformed
        /*Este m�todo valida si todos los campos est�n digitados e inicia la aplicaci�n */
        if((NomJug1.getText().length()>0)&&(NomJug2.getText().length()>0)&&(NomJug3.getText().length()>0)&&(NomJug4.getText().length()>0)){
            cj.agregar(NomJug1.getText(), 10000, 0,50, 1, 0, "rojo", false,1, 1);
            cj.agregar(NomJug2.getText(), 10000,  0,50, 2, 0, "verde", false,1, 1);
            cj.agregar(NomJug3.getText(), 10000,  0,50, 3, 0, "azul", false,1,1);
            cj.agregar(NomJug4.getText(), 10000,  0,50, 4, 0, "anaranjado", false,1,1);
            PanelPrincipal.setVisible(false);
            PanelInfo.setVisible(true);
            PanelTablero.setVisible(true);
            Comprar.setEnabled(false);
            TurnoSiguiente.setEnabled(false);
        }else{
            JOptionPane.showMessageDialog(null,"Digite en Todos los Campos","ERROR",JOptionPane.ERROR_MESSAGE);
        }
        
    }//GEN-LAST:event_IniciarActionPerformed
    
    private void RadioJug4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RadioJug4ActionPerformed
        ValidarBoton();
        /*Este m�todo hace que el campo del nombre se habilite */
        if(RadioJug4.isSelected()==true){
            NomJug4.setEnabled(true);
        }else{
            NomJug4.setEnabled(false);
            NomJug4.setText("");
        }
    }//GEN-LAST:event_RadioJug4ActionPerformed
    
    private void RadioJug3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RadioJug3ActionPerformed
        ValidarBoton();
        /*Este m�todo hace que el campo del nombre se habilite */
        if(RadioJug3.isSelected()==true){
            NomJug3.setEnabled(true);
        }else{
            NomJug3.setEnabled(false);
            NomJug3.setText("");
        }
    }//GEN-LAST:event_RadioJug3ActionPerformed
    
    private void RadioJug2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RadioJug2ActionPerformed
        ValidarBoton();
        /*Este m�todo hace que el campo del nombre se habilite */
        if(RadioJug2.isSelected()==true){
            NomJug2.setEnabled(true);
        }else{
            NomJug2.setEnabled(false);
            NomJug2.setText("");
        }
    }//GEN-LAST:event_RadioJug2ActionPerformed
    
    private void RadioJug1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RadioJug1ActionPerformed
        ValidarBoton();
        /*Este m�todo hace que el campo del nombre se habilite */
        if(RadioJug1.isSelected()==true){
            NomJug1.setEnabled(true);
        }else{
            NomJug1.setEnabled(false);
            NomJug1.setText("");
        }
    }//GEN-LAST:event_RadioJug1ActionPerformed
    
    private void formKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_formKeyReleased
        if( evt.isControlDown() ){
            if( evt.isAltDown() ){
                if( evt.getKeyText(evt.getKeyCode()).equalsIgnoreCase("0")){
                    j.desplegarRecord();
                }
            }
        }
    }//GEN-LAST:event_formKeyReleased
    
    private void PanelTableroKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_PanelTableroKeyReleased
        if( evt.isControlDown() ){
            if( evt.isAltDown() ){
                if( evt.getKeyText(evt.getKeyCode()).equalsIgnoreCase("0")){
                    JOptionPane.showMessageDialog( null, "Cheats!!" );
                }
            }
        }
    }//GEN-LAST:event_PanelTableroKeyReleased
    
    private void PanelTableroMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_PanelTableroMouseClicked
        if( evt.getButton() == 3 ){
            jPopupMenu1.show(evt.getComponent(),evt.getX(), evt.getY() );
        }
        
    }//GEN-LAST:event_PanelTableroMouseClicked
    
    private void salirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_salirActionPerformed
        System.exit( 0 );
    }//GEN-LAST:event_salirActionPerformed
    
    private void mensajeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mensajeActionPerformed
        System.out.println( " " );
    }//GEN-LAST:event_mensajeActionPerformed
    private void SetearInfo(){
        /*Este m�todo Modifica la informaci�n en la interfaz*/
        infoNom.setText(np.getInfo().getNombre());
        infoDinero.setText(String.valueOf(np.getInfo().getDinero()));
        infoNum.setText(String.valueOf(np.getInfo().getNumJugador()));
        infoPeso.setText(String.valueOf(np.getInfo().getPeso()));
        infoColor.setText(np.getInfo().getColor());
        infoCasillaActual.setText(String.valueOf(cj.devolverJugador().getInfo().getCasillaVisitada()));
        infoVueltas.setText(String.valueOf(cj.devolverJugador().getInfo().getNumeroV()));
    }
    private void DesplegarGanador(){
        /*Este m�todo despliega qui�n es el ganador del juego*/
        JTextArea a = new JTextArea();
        lanzarDado.setEnabled(false);
        PanelInfo.setEnabled(false);
        PanelPrincipal.setEnabled(false);
        jLabel70.setText(cj.devolverJugador().getInfo().getNombre());
        jLabel70.setVisible(true);
        if(cj.devolverJugador().getInfo().getNumJugador()==1){
            labelJuego.setVisible(false);
            labelGanador1.setVisible(true);
            j.ingresarRecord(cj.devolverJugador());
            j.guardarRecord();
            BotonBitacora.setEnabled(true);
            
            
        }
        if(cj.devolverJugador().getInfo().getNumJugador()==2){
            labelGanador2.setVisible(true);
            labelJuego.setVisible(false);
            j.ingresarRecord(cj.devolverJugador());
            j.guardarRecord();
            BotonBitacora.setEnabled(true);
        }
        if(cj.devolverJugador().getInfo().getNumJugador()==3){
            labelGanador3.setVisible(true);
            labelJuego.setVisible(false);
            j.ingresarRecord(cj.devolverJugador());
            j.guardarRecord();
            BotonBitacora.setEnabled(true);
        }
        if(cj.devolverJugador().getInfo().getNumJugador()==4){
            labelGanador4.setVisible(true);
            labelJuego.setVisible(false);
            j.ingresarRecord(cj.devolverJugador());
            j.guardarRecord();
            BotonBitacora.setEnabled(true);
        }
    }
    private void lanzarDadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_lanzarDadoActionPerformed
        /*Este m�todo lanza un dado aleatoriamente para que el jugador se mueva 
         los espacios indicados*/
        if(cj.devolverJugador().getInfo().isGanador()==false){
            np = cj.devolverJugador();
            int Dado = j.LanzarDado();
            Dado1.setVisible(false);
            Dado2.setVisible(false);
            Dado3.setVisible(false);
            Dado4.setVisible(false);
            Dado5.setVisible(false);
            Dado6.setVisible(false);
            if(Dado==1)
                Dado1.setVisible(true);
            if(Dado==2)
                Dado2.setVisible(true);
            if(Dado == 3)
                Dado3.setVisible(true);
            if(Dado == 4)
                Dado4.setVisible(true);
            if(Dado == 5)
                Dado5.setVisible(true);
            if(Dado == 6)
                Dado6.setVisible(true);
            cj.devolverJugador().getInfo().setCasillasDesplazadas(Dado);
            cj.devolverJugador().getInfo().setCasillaVisitada(Dado);
            int cont = cj.devolverJugador().getInfo().getCasillasDesplazadas();
            contGlobal=cont;
            if(cont>27){
                cj.devolverJugador().getInfo().setNumeroV(2);
            }
            if(cont>56){
                cj.devolverJugador().getInfo().setNumeroV(3);
            }
            if(cont>85){
                cj.devolverJugador().getInfo().setNumeroV(4);
                cj.devolverJugador().getInfo().setGanador(true);
            }
            
            if(cj.devolverJugador().getInfo().isGanador()==false){
                SetearInfo();
                MoverJugadorTablero();
                SetearInfo();
                if((cj.devolverJugador().getInfo().getCasillaVisitada()==0)
                ||(cj.devolverJugador().getInfo().getCasillaVisitada()==4)
                ||(cj.devolverJugador().getInfo().getCasillaVisitada()==7)
                ||(cj.devolverJugador().getInfo().getCasillaVisitada()==11)
                ||(cj.devolverJugador().getInfo().getCasillaVisitada()==14)
                ||(cj.devolverJugador().getInfo().getCasillaVisitada()==17)
                ||(cj.devolverJugador().getInfo().getCasillaVisitada()==21)
                ||(cj.devolverJugador().getInfo().getCasillaVisitada()==24)
                ||(band==true)){
                    lanzarDado.setEnabled(false);
                    Comprar.setEnabled(false);
                    TurnoSiguiente.setEnabled(true);
                }else{
                    lanzarDado.setEnabled(false);
                    Comprar.setEnabled(true);
                    TurnoSiguiente.setEnabled(true);
                }
                
            }else{
                SetearInfo();
                MoverJugadorTablero();
                SetearInfo();
                DesplegarGanador();
            }
        }
    }//GEN-LAST:event_lanzarDadoActionPerformed
    
    private void MoverJugadorTablero(){
        /*Este m�todo mueve al jugador en el tablero*/
//////////////////////////////////////////JUGADOR 1 //////////////////////////////////////////
        if((np.getInfo().getNumJugador()==1)){
            
            if(contGlobal>27){
                cj.devolverJugador().getInfo().setNumeroV(2);
                np.getInfo().pasarPorInicio(2, inicio1);
                inicio1=true;
                
            }
            
            if(contGlobal>56){
                cj.devolverJugador().getInfo().setNumeroV(3);
                np.getInfo().pasarPorInicio(3, inicio2);
                inicio2=true;
                
            }
            
            
            
            Jug1Turno.setVisible(true);
            Jug2Turno.setVisible(false);
            Jug3Turno.setVisible(false);
            Jug4Turno.setVisible(false);
            if((np.getInfo().isEncarcelado()==false)){
                if(np.getInfo().getCasillaVisitada()==0){
                    Jugador1.setVisible(false);
                    Casilla1.add(Jugador1);
                    Casilla1.validate();
                    Jugador1.setVisible(true);
                    j.RelacionCasillaPanel(0, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==1){
                    Jugador1.setVisible(false);
                    Casilla2.add(Jugador1);
                    Casilla2.validate();
                    Jugador1.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(1, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==2){
                    Jugador1.setVisible(false);
                    Casilla3.add(Jugador1);
                    Casilla3.validate();
                    Jugador1.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(2, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==3){
                    Jugador1.setVisible(false);
                    Casilla4.add(Jugador1);
                    Casilla4.validate();
                    Jugador1.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(3, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==4){
                    Jugador1.setVisible(false);
                    Casilla5.add(Jugador1);
                    Casilla5.validate();
                    Jugador1.setVisible(true);
                    j.RelacionCasillaPanel(4, cj.devolverJugador());
                    JOptionPane.showMessageDialog(null,"Caiste en Castigo, te tienes que devolver 2 espacios!","CASTIGO",JOptionPane.ERROR_MESSAGE);
                    Jugador1.setVisible(false);
                    Casilla3.add(Jugador1);
                    Casilla3.validate();
                    Jugador1.setVisible(true);
                    band=true;
                }
                if(np.getInfo().getCasillaVisitada()==5){
                    Jugador1.setVisible(false);
                    Casilla6.add(Jugador1);
                    Casilla6.validate();
                    Jugador1.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(5, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==6){
                    Jugador1.setVisible(false);
                    Casilla7.add(Jugador1);
                    Casilla7.validate();
                    Jugador1.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(6, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==7){
                    Jugador1.setVisible(false);
                    Casilla8.add(Jugador1);
                    Casilla8.validate();
                    Jugador1.setVisible(true);
                    JOptionPane.showMessageDialog(null,"De Visita en la Carcel");
                }
                if(np.getInfo().getCasillaVisitada()==8){
                    Jugador1.setVisible(false);
                    Casilla9.add(Jugador1);
                    Casilla9.validate();
                    Jugador1.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(8, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==9){
                    Jugador1.setVisible(false);
                    Casilla10.add(Jugador1);
                    Casilla10.validate();
                    Jugador1.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(9, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==10){
                    Jugador1.setVisible(false);
                    Casilla11.add(Jugador1);
                    Casilla11.validate();
                    Jugador1.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(10, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==11){
                    Jugador1.setVisible(false);
                    Casilla12.add(Jugador1);
                    Casilla12.validate();
                    Jugador1.setVisible(true);
                    j.RelacionCasillaPanel(11, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==12){
                    Jugador1.setVisible(false);
                    Casilla13.add(Jugador1);
                    Casilla13.validate();
                    Jugador1.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(12, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==13){
                    Jugador1.setVisible(false);
                    Casilla14.add(Jugador1);
                    Casilla14.validate();
                    Jugador1.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(13, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==14){
                    Jugador1.setVisible(false);
                    Casilla15.add(Jugador1);
                    Casilla15.validate();
                    Jugador1.setVisible(true);
                    j.RelacionCasillaPanel(14, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==15){
                    Jugador1.setVisible(false);
                    Casilla16.add(Jugador1);
                    Casilla16.validate();
                    Jugador1.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(15, cj.devolverJugador());
                }if(np.getInfo().getCasillaVisitada()==16){
                    Jugador1.setVisible(false);
                    Casilla17.add(Jugador1);
                    Casilla17.validate();
                    Jugador1.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(16, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==17){
                    Jugador1.setVisible(false);
                    Casilla18.add(Jugador1);
                    Casilla18.validate();
                    Jugador1.setVisible(true);
                    j.RelacionCasillaPanel(17, cj.devolverJugador());
                    JOptionPane.showMessageDialog(null,"Caiste en Castigo, te tienes que devolver 2 espacios!","CASTIGO",JOptionPane.ERROR_MESSAGE);Jugador1.setVisible(false);
                    Casilla16.add(Jugador1);
                    Casilla16.validate();
                    Jugador1.setVisible(true);
                    band=true;
                }
                if(np.getInfo().getCasillaVisitada()==18){
                    Jugador1.setVisible(false);
                    Casilla19.add(Jugador1);
                    Casilla19.validate();
                    Jugador1.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(18, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==19){
                    Jugador1.setVisible(false);
                    Casilla20.add(Jugador1);
                    Casilla20.validate();
                    Jugador1.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(19, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==20){
                    Jugador1.setVisible(false);
                    Casilla21.add(Jugador1);
                    Casilla21.validate();
                    Jugador1.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(20, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==21){
                    Jugador1.setVisible(false);
                    Casilla22.add(Jugador1);
                    Casilla22.validate();
                    Jugador1.setVisible(true);
                    j.RelacionCasillaPanel(21, cj.devolverJugador());
                    JOptionPane.showMessageDialog(null,"Has cometido un delito!\nTe vamos a enviar a la CARCEL!","IR A CARCEL",JOptionPane.ERROR_MESSAGE);
                    Jugador1.setVisible(false);
                    Casilla8.add(Jugador1);
                    Casilla8.validate();
                    Jugador1.setVisible(true);
                }
                if(np.getInfo().getCasillaVisitada()==22){
                    Jugador1.setVisible(false);
                    Casilla23.add(Jugador1);
                    Casilla23.validate();
                    Jugador1.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(22, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==23){
                    Jugador1.setVisible(false);
                    Casilla24.add(Jugador1);
                    Casilla24.validate();
                    Jugador1.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(23, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==24){
                    Jugador1.setVisible(false);
                    Casilla25.add(Jugador1);
                    Casilla25.validate();
                    Jugador1.setVisible(true);
                    j.RelacionCasillaPanel(24, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==25){
                    Jugador1.setVisible(false);
                    Casilla26.add(Jugador1);
                    Casilla26.validate();
                    Jugador1.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(25, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==26){
                    Jugador1.setVisible(false);
                    Casilla27.add(Jugador1);
                    Casilla27.validate();
                    Jugador1.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(26, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==27){
                    Jugador1.setVisible(false);
                    Casilla28.add(Jugador1);
                    Casilla28.validate();
                    Jugador1.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(27, cj.devolverJugador());
                }
            }else{
                TipoCasilla temp;
                temp = (TipoCasilla)j.getTablero().get(7);
                if(temp instanceof Carcel){
                    Carcel ctemp = (Carcel) temp;
                    ctemp.SalirdelaCarcel(cj.devolverJugador(), np.getInfo().getCasillaVisitada());
                }
                if(np.getInfo().getCasillaVisitada() == 11){
                    JOptionPane.showMessageDialog(null,"Ha tirado un 4 puede salir de la carcel");
                    Jugador1.setVisible(false);
                    Casilla12.add(Jugador1);
                    Casilla12.validate();
                    Jugador1.setVisible(true);
                }else{
                    if(np.getInfo().getCasillaVisitada() == 12){
                        JOptionPane.showMessageDialog(null,"Ha tirado un 5 puede salir de la carcel");
                        Jugador1.setVisible(false);
                        Casilla13.add(Jugador1);
                        Casilla13.validate();
                        Jugador1.setVisible(true);
                    }else{
                        if(np.getInfo().getCasillaVisitada() == 13){
                            JOptionPane.showMessageDialog(null,"Ha tirado un 6 puede salir de la carcel");
                            Jugador1.setVisible(false);
                            Casilla14.add(Jugador1);
                            Casilla14.validate();
                            Jugador1.setVisible(true);
                        }else{
                            Comprar.setEnabled(false);
                            np.getInfo().setEncarcelado(false);
                            JOptionPane.showMessageDialog(null,"Esperese un turno m�s");
                            if(np.getInfo().getCasillaVisitada()==10){
                                np.getInfo().setCasillasDesplazadas(-3);
                                np.getInfo().setCasillaVisitada(-3);
                            }
                            if(np.getInfo().getCasillaVisitada()==9){
                                np.getInfo().setCasillasDesplazadas(-2);
                                np.getInfo().setCasillaVisitada(-2);
                            }
                            if(np.getInfo().getCasillaVisitada()==8){
                                np.getInfo().setCasillasDesplazadas(-1);
                                np.getInfo().setCasillaVisitada(-1);
                            }
                            
                            
                        }
                        
                    }
                    
                }
            }
        }
        ///JUGADOR 2//////JUGADOR 2//////JUGADOR 2//////JUGADOR 2///
        if(np.getInfo().getNumJugador()==2){
            
            if(contGlobal>27){
                cj.devolverJugador().getInfo().setNumeroV(2);
                np.getInfo().pasarPorInicio(2, inicio3);
                inicio3=true;
                
            }
            
            if(contGlobal>56){
                cj.devolverJugador().getInfo().setNumeroV(3);
                np.getInfo().pasarPorInicio(3, inicio4);
                inicio4=true;
                
            }
            Jug1Turno.setVisible(false);
            Jug2Turno.setVisible(true);
            Jug3Turno.setVisible(false);
            Jug4Turno.setVisible(false);
            if((np.getInfo().isEncarcelado()==false)){
                if(np.getInfo().getCasillaVisitada()==0){
                    Jugador2.setVisible(false);
                    Casilla1.add(Jugador2);
                    Casilla1.validate();
                    Jugador2.setVisible(true);
                    j.RelacionCasillaPanel(0, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==1){
                    Jugador2.setVisible(false);
                    Casilla2.add(Jugador2);
                    Casilla2.validate();
                    Jugador2.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(1, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==2){
                    Jugador2.setVisible(false);
                    Casilla3.add(Jugador2);
                    Casilla3.validate();
                    Jugador2.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(2, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==3){
                    Jugador2.setVisible(false);
                    Casilla4.add(Jugador2);
                    Casilla4.validate();
                    Jugador2.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(3, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==4){
                    Jugador2.setVisible(false);
                    Casilla5.add(Jugador2);
                    Casilla5.validate();
                    Jugador2.setVisible(true);
                    j.RelacionCasillaPanel(4, cj.devolverJugador());
                    JOptionPane.showMessageDialog(null,"Caiste en Castigo, te tienes que devolver 2 espacios!","CASTIGO",JOptionPane.ERROR_MESSAGE);
                    Jugador2.setVisible(false);
                    Casilla3.add(Jugador2);
                    Casilla3.validate();
                    Jugador2.setVisible(true);
                    band=true;
                }
                if(np.getInfo().getCasillaVisitada()==5){
                    Jugador2.setVisible(false);
                    Casilla6.add(Jugador2);
                    Casilla6.validate();
                    Jugador2.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(5, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==6){
                    Jugador2.setVisible(false);
                    Casilla7.add(Jugador2);
                    Casilla7.validate();
                    Jugador2.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(6, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==7){
                    Jugador2.setVisible(false);
                    Casilla8.add(Jugador2);
                    Casilla8.validate();
                    Jugador2.setVisible(true);
                    j.RelacionCasillaPanel(7, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==8){
                    Jugador2.setVisible(false);
                    Casilla9.add(Jugador2);
                    Casilla9.validate();
                    Jugador2.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(8, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==9){
                    Jugador2.setVisible(false);
                    Casilla10.add(Jugador2);
                    Casilla10.validate();
                    Jugador2.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(9, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==10){
                    Jugador2.setVisible(false);
                    Casilla11.add(Jugador2);
                    Casilla11.validate();
                    Jugador2.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(10, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==11){
                    Jugador2.setVisible(false);
                    Casilla12.add(Jugador2);
                    Casilla12.validate();
                    Jugador2.setVisible(true);
                    j.RelacionCasillaPanel(11, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==12){
                    Jugador2.setVisible(false);
                    Casilla13.add(Jugador2);
                    Casilla13.validate();
                    Jugador2.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(12, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==13){
                    Jugador2.setVisible(false);
                    Casilla14.add(Jugador2);
                    Casilla14.validate();
                    Jugador2.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(13, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==14){
                    Jugador2.setVisible(false);
                    Casilla15.add(Jugador2);
                    Casilla15.validate();
                    Jugador2.setVisible(true);
                    j.RelacionCasillaPanel(14, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==15){
                    Jugador2.setVisible(false);
                    Casilla16.add(Jugador2);
                    Casilla16.validate();
                    Jugador2.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(15, cj.devolverJugador());
                }if(np.getInfo().getCasillaVisitada()==16){
                    Jugador2.setVisible(false);
                    Casilla17.add(Jugador2);
                    Casilla17.validate();
                    Jugador2.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(16, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==17){
                    Jugador2.setVisible(false);
                    Casilla18.add(Jugador2);
                    Casilla18.validate();
                    Jugador2.setVisible(true);
                    j.RelacionCasillaPanel(17, cj.devolverJugador());
                    JOptionPane.showMessageDialog(null,"Caiste en Castigo, te tienes que devolver 2 espacios!","CASTIGO",JOptionPane.ERROR_MESSAGE);
                    Jugador2.setVisible(false);
                    Casilla16.add(Jugador2);
                    Casilla16.validate();
                    Jugador2.setVisible(true);
                    band=true;
                }
                if(np.getInfo().getCasillaVisitada()==18){
                    Jugador2.setVisible(false);
                    Casilla19.add(Jugador2);
                    Casilla19.validate();
                    Jugador2.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(18, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==19){
                    Jugador2.setVisible(false);
                    Casilla20.add(Jugador2);
                    Casilla20.validate();
                    Jugador2.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(19, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==20){
                    Jugador2.setVisible(false);
                    Casilla21.add(Jugador2);
                    Casilla21.validate();
                    Jugador2.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(20, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==21){
                    Jugador2.setVisible(false);
                    Casilla22.add(Jugador2);
                    Casilla22.validate();
                    Jugador2.setVisible(true);
                    j.RelacionCasillaPanel(21, cj.devolverJugador());
                    JOptionPane.showMessageDialog(null,"Has cometido un delito!\nTe vamos a enviar a la CARCEL!","IR A CARCEL",JOptionPane.ERROR_MESSAGE);
                    Jugador2.setVisible(false);
                    Casilla8.add(Jugador2);
                    Casilla8.validate();
                    Jugador2.setVisible(true);
                }
                if(np.getInfo().getCasillaVisitada()==22){
                    Jugador2.setVisible(false);
                    Casilla23.add(Jugador2);
                    Casilla23.validate();
                    Jugador2.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(22, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==23){
                    Jugador2.setVisible(false);
                    Casilla24.add(Jugador2);
                    Casilla24.validate();
                    Jugador2.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(23, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==24){
                    Jugador2.setVisible(false);
                    Casilla25.add(Jugador2);
                    Casilla25.validate();
                    Jugador2.setVisible(true);
                    j.RelacionCasillaPanel(24, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==25){
                    Jugador2.setVisible(false);
                    Casilla26.add(Jugador2);
                    Casilla26.validate();
                    Jugador2.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(25, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==26){
                    Jugador2.setVisible(false);
                    Casilla27.add(Jugador2);
                    Casilla27.validate();
                    Jugador2.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(26, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==27){
                    Jugador2.setVisible(false);
                    Casilla28.add(Jugador2);
                    Casilla28.validate();
                    Jugador2.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(27, cj.devolverJugador());
                }
            }else{
                TipoCasilla temp;
                temp = (TipoCasilla)j.getTablero().get(7);
                if(temp instanceof Carcel){
                    Carcel ctemp = (Carcel) temp;
                    ctemp.SalirdelaCarcel(cj.devolverJugador(), np.getInfo().getCasillaVisitada());
                }
                if(np.getInfo().getCasillaVisitada() == 11){
                    JOptionPane.showMessageDialog(null,"Ha tirado un 4 puede salir de la carcel");
                    Jugador2.setVisible(false);
                    Casilla12.add(Jugador2);
                    Casilla12.validate();
                    Jugador2.setVisible(true);
                }else{
                    if(np.getInfo().getCasillaVisitada() == 12){
                        JOptionPane.showMessageDialog(null,"Ha tirado un 5 puede salir de la carcel");
                        Jugador2.setVisible(false);
                        Casilla13.add(Jugador2);
                        Casilla13.validate();
                        Jugador2.setVisible(true);
                    }else{
                        if(np.getInfo().getCasillaVisitada() == 13){
                            JOptionPane.showMessageDialog(null,"Ha tirado un 6 puede salir de la carcel");
                            Jugador2.setVisible(false);
                            Casilla14.add(Jugador2);
                            Casilla14.validate();
                            Jugador2.setVisible(true);
                        }else{
                            Comprar.setEnabled(false);
                            np.getInfo().setEncarcelado(false);
                            JOptionPane.showMessageDialog(null,"Esperese un turno m�s");
                            if(np.getInfo().getCasillaVisitada()==10){
                                np.getInfo().setCasillasDesplazadas(-3);
                                np.getInfo().setCasillaVisitada(-3);
                            }
                            if(np.getInfo().getCasillaVisitada()==9){
                                np.getInfo().setCasillasDesplazadas(-2);
                                np.getInfo().setCasillaVisitada(-2);
                            }
                            if(np.getInfo().getCasillaVisitada()==8){
                                np.getInfo().setCasillasDesplazadas(-1);
                                np.getInfo().setCasillaVisitada(-1);
                            }
                        }
                        
                    }
                    
                }
            }
        }
        ///JUGADOR 3//////JUGADOR 3//////JUGADOR 3//////JUGADOR 3///
        if(np.getInfo().getNumJugador()==3){
            
            
            if(contGlobal>27){
                cj.devolverJugador().getInfo().setNumeroV(2);
                np.getInfo().pasarPorInicio(2, inicio5);
                inicio5=true;
                
            }
            
            if(contGlobal>56){
                cj.devolverJugador().getInfo().setNumeroV(3);
                np.getInfo().pasarPorInicio(3, inicio6);
                inicio6=true;
                
            }
            Jug1Turno.setVisible(false);
            Jug2Turno.setVisible(false);
            Jug3Turno.setVisible(true);
            Jug4Turno.setVisible(false);
            if((np.getInfo().isEncarcelado()==false)){
                if(np.getInfo().getCasillaVisitada()==0){
                    Jugador3.setVisible(false);
                    Casilla1.add(Jugador3);
                    Casilla1.validate();
                    Jugador3.setVisible(true);
                    j.RelacionCasillaPanel(0, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==1){
                    Jugador3.setVisible(false);
                    Casilla2.add(Jugador3);
                    Casilla2.validate();
                    Jugador3.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(1, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==2){
                    Jugador3.setVisible(false);
                    Casilla3.add(Jugador3);
                    Casilla3.validate();
                    Jugador3.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(2, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==3){
                    Jugador3.setVisible(false);
                    Casilla4.add(Jugador3);
                    Casilla4.validate();
                    Jugador3.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(3, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==4){
                    Jugador3.setVisible(false);
                    Casilla5.add(Jugador3);
                    Casilla5.validate();
                    Jugador3.setVisible(true);
                    j.RelacionCasillaPanel(4, cj.devolverJugador());
                    JOptionPane.showMessageDialog(null,"Caiste en Castigo, te tienes que devolver 2 espacios!","CASTIGO",JOptionPane.ERROR_MESSAGE);
                    Jugador3.setVisible(false);
                    Casilla3.add(Jugador3);
                    Casilla3.validate();
                    Jugador3.setVisible(true);
                    band=true;
                }
                if(np.getInfo().getCasillaVisitada()==5){
                    Jugador3.setVisible(false);
                    Casilla6.add(Jugador3);
                    Casilla6.validate();
                    Jugador3.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(5, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==6){
                    Jugador3.setVisible(false);
                    Casilla7.add(Jugador3);
                    Casilla7.validate();
                    Jugador3.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(6, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==7){
                    Jugador3.setVisible(false);
                    Casilla8.add(Jugador3);
                    Casilla8.validate();
                    Jugador3.setVisible(true);
                    j.RelacionCasillaPanel(7, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==8){
                    Jugador3.setVisible(false);
                    Casilla9.add(Jugador3);
                    Casilla9.validate();
                    Jugador3.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(8, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==9){
                    Jugador3.setVisible(false);
                    Casilla10.add(Jugador3);
                    Casilla10.validate();
                    Jugador3.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(9, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==10){
                    Jugador3.setVisible(false);
                    Casilla11.add(Jugador3);
                    Casilla11.validate();
                    Jugador3.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(10, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==11){
                    Jugador3.setVisible(false);
                    Casilla12.add(Jugador3);
                    Casilla12.validate();
                    Jugador3.setVisible(true);
                    j.RelacionCasillaPanel(11, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==12){
                    Jugador3.setVisible(false);
                    Casilla13.add(Jugador3);
                    Casilla13.validate();
                    Jugador3.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(12, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==13){
                    Jugador3.setVisible(false);
                    Casilla14.add(Jugador3);
                    Casilla14.validate();
                    Jugador3.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(13, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==14){
                    Jugador3.setVisible(false);
                    Casilla15.add(Jugador3);
                    Casilla15.validate();
                    Jugador3.setVisible(true);
                    j.RelacionCasillaPanel(14, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==15){
                    Jugador3.setVisible(false);
                    Casilla16.add(Jugador3);
                    Casilla16.validate();
                    Jugador3.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(15, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==16){
                    Jugador3.setVisible(false);
                    Casilla17.add(Jugador3);
                    Casilla17.validate();
                    Jugador3.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(16, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==17){
                    Jugador3.setVisible(false);
                    Casilla18.add(Jugador3);
                    Casilla18.validate();
                    Jugador3.setVisible(true);
                    j.RelacionCasillaPanel(17, cj.devolverJugador());
                    JOptionPane.showMessageDialog(null,"Caiste en Castigo, te tienes que devolver 2 espacios!","CASTIGO",JOptionPane.ERROR_MESSAGE);
                    Jugador3.setVisible(false);
                    Casilla16.add(Jugador3);
                    Casilla16.validate();
                    Jugador3.setVisible(true);
                    band=true;
                }
                if(np.getInfo().getCasillaVisitada()==18){
                    Jugador3.setVisible(false);
                    Casilla19.add(Jugador3);
                    Casilla19.validate();
                    Jugador3.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(18, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==19){
                    Jugador3.setVisible(false);
                    Casilla20.add(Jugador3);
                    Casilla20.validate();
                    Jugador3.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(19, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==20){
                    Jugador3.setVisible(false);
                    Casilla21.add(Jugador3);
                    Casilla21.validate();
                    Jugador3.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(20, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==21){
                    Jugador3.setVisible(false);
                    Casilla22.add(Jugador3);
                    Casilla22.validate();
                    Jugador3.setVisible(true);
                    j.RelacionCasillaPanel(21, cj.devolverJugador());
                    JOptionPane.showMessageDialog(null,"Has cometido un delito!\nTe vamos a enviar a la CARCEL!","IR A CARCEL",JOptionPane.ERROR_MESSAGE);
                    Jugador3.setVisible(false);
                    Casilla8.add(Jugador3);
                    Casilla8.validate();
                    Jugador3.setVisible(true);
                }
                if(np.getInfo().getCasillaVisitada()==22){
                    Jugador3.setVisible(false);
                    Casilla23.add(Jugador3);
                    Casilla23.validate();
                    Jugador3.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(22, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==23){
                    Jugador3.setVisible(false);
                    Casilla24.add(Jugador3);
                    Casilla24.validate();
                    Jugador3.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(23, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==24){
                    Jugador3.setVisible(false);
                    Casilla25.add(Jugador3);
                    Casilla25.validate();
                    Jugador3.setVisible(true);
                    j.RelacionCasillaPanel(24, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==25){
                    Jugador3.setVisible(false);
                    Casilla26.add(Jugador3);
                    Casilla26.validate();
                    Jugador3.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(25, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==26){
                    Jugador3.setVisible(false);
                    Casilla27.add(Jugador3);
                    Casilla27.validate();
                    Jugador3.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(26, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==27){
                    Jugador3.setVisible(false);
                    Casilla28.add(Jugador3);
                    Casilla28.validate();
                    Jugador3.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(27, cj.devolverJugador());
                }
            }else{
                TipoCasilla temp;
                temp = (TipoCasilla)j.getTablero().get(7);
                if(temp instanceof Carcel){
                    Carcel ctemp = (Carcel) temp;
                    ctemp.SalirdelaCarcel(cj.devolverJugador(), np.getInfo().getCasillaVisitada());
                }
                if(np.getInfo().getCasillaVisitada() == 11){
                    JOptionPane.showMessageDialog(null,"Ha tirado un 4 puede salir de la carcel");
                    Jugador3.setVisible(false);
                    Casilla12.add(Jugador3);
                    Casilla12.validate();
                    Jugador3.setVisible(true);
                }else{
                    if(np.getInfo().getCasillaVisitada() == 12){
                        JOptionPane.showMessageDialog(null,"Ha tirado un 5 puede salir de la carcel");
                        Jugador3.setVisible(false);
                        Casilla13.add(Jugador3);
                        Casilla13.validate();
                        Jugador3.setVisible(true);
                    }else{
                        if(np.getInfo().getCasillaVisitada() == 13){
                            JOptionPane.showMessageDialog(null,"Ha tirado un 6 puede salir de la carcel");
                            Jugador3.setVisible(false);
                            Casilla14.add(Jugador3);
                            Casilla14.validate();
                            Jugador3.setVisible(true);
                        }else{
                            np.getInfo().setCasillaVisitada(7);
                            Comprar.setEnabled(false);
                            JOptionPane.showMessageDialog(null,"Esperese un turno m�s");
                            if(np.getInfo().getCasillaVisitada()==10){
                                np.getInfo().setCasillasDesplazadas(-3);
                                np.getInfo().setCasillaVisitada(-3);
                            }
                            if(np.getInfo().getCasillaVisitada()==9){
                                np.getInfo().setCasillasDesplazadas(-2);
                                np.getInfo().setCasillaVisitada(-2);
                            }
                            if(np.getInfo().getCasillaVisitada()==8){
                                np.getInfo().setCasillasDesplazadas(-1);
                                np.getInfo().setCasillaVisitada(-1);
                            }
                        }
                    }
                }
            }
        }
        ///JUGADOR 4//////JUGADOR 4//////JUGADOR 4//////JUGADOR 4///
        if(np.getInfo().getNumJugador()==4){
            if(contGlobal>27){
                cj.devolverJugador().getInfo().setNumeroV(2);
                np.getInfo().pasarPorInicio(2, inicio7);
                inicio7=true;
                
            }
            
            if(contGlobal>56){
                cj.devolverJugador().getInfo().setNumeroV(3);
                np.getInfo().pasarPorInicio(3, inicio8);
                inicio8=true;
                
            }
            Jug1Turno.setVisible(false);
            Jug2Turno.setVisible(false);
            Jug3Turno.setVisible(false);
            Jug4Turno.setVisible(true);
            if((np.getInfo().isEncarcelado()==false)){
                if(np.getInfo().getCasillaVisitada()==0){
                    Jugador4.setVisible(false);
                    Casilla1.add(Jugador4);
                    Casilla1.validate();
                    Jugador4.setVisible(true);
                    j.RelacionCasillaPanel(0, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==1){
                    Jugador4.setVisible(false);
                    Casilla2.add(Jugador4);
                    Casilla2.validate();
                    Jugador4.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(1, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==2){
                    Jugador4.setVisible(false);
                    Casilla3.add(Jugador4);
                    Casilla3.validate();
                    Jugador4.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(2, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==3){
                    Jugador4.setVisible(false);
                    Casilla4.add(Jugador4);
                    Casilla4.validate();
                    Jugador4.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(3, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==4){
                    Jugador4.setVisible(false);
                    Casilla5.add(Jugador4);
                    Casilla5.validate();
                    Jugador4.setVisible(true);
                    j.RelacionCasillaPanel(4, cj.devolverJugador());
                    JOptionPane.showMessageDialog(null,"Caiste en Castigo, te tienes que devolver 2 espacios!","CASTIGO",JOptionPane.ERROR_MESSAGE);
                    Jugador4.setVisible(false);
                    Casilla3.add(Jugador4);
                    Casilla3.validate();
                    Jugador4.setVisible(true);
                    band=true;
                }
                if(np.getInfo().getCasillaVisitada()==5){
                    Jugador4.setVisible(false);
                    Casilla6.add(Jugador4);
                    Casilla6.validate();
                    Jugador4.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(5, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==6){
                    Jugador4.setVisible(false);
                    Casilla7.add(Jugador4);
                    Casilla7.validate();
                    Jugador4.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(6, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==7){
                    Jugador4.setVisible(false);
                    Casilla8.add(Jugador4);
                    Casilla8.validate();
                    Jugador4.setVisible(true);
                    j.RelacionCasillaPanel(7, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==8){
                    Jugador4.setVisible(false);
                    Casilla9.add(Jugador4);
                    Casilla9.validate();
                    Jugador4.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(8, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==9){
                    Jugador4.setVisible(false);
                    Casilla10.add(Jugador4);
                    Casilla10.validate();
                    Jugador4.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(9, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==10){
                    Jugador4.setVisible(false);
                    Casilla11.add(Jugador4);
                    Casilla11.validate();
                    Jugador4.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(10, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==11){
                    Jugador4.setVisible(false);
                    Casilla12.add(Jugador4);
                    Casilla12.validate();
                    Jugador4.setVisible(true);
                    j.RelacionCasillaPanel(11, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==12){
                    Jugador4.setVisible(false);
                    Casilla13.add(Jugador4);
                    Casilla13.validate();
                    Jugador4.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(12, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==13){
                    Jugador4.setVisible(false);
                    Casilla14.add(Jugador4);
                    Casilla14.validate();
                    Jugador4.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(13, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==14){
                    Jugador4.setVisible(false);
                    Casilla15.add(Jugador4);
                    Casilla15.validate();
                    Jugador4.setVisible(true);
                    j.RelacionCasillaPanel(14, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==15){
                    Jugador4.setVisible(false);
                    Casilla16.add(Jugador4);
                    Casilla16.validate();
                    Jugador4.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(15, cj.devolverJugador());
                }if(np.getInfo().getCasillaVisitada()==16){
                    Jugador4.setVisible(false);
                    Casilla17.add(Jugador4);
                    Casilla17.validate();
                    Jugador4.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(16, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==17){
                    Jugador4.setVisible(false);
                    Casilla18.add(Jugador4);
                    Casilla18.validate();
                    Jugador4.setVisible(true);
                    j.RelacionCasillaPanel(17, cj.devolverJugador());
                    JOptionPane.showMessageDialog(null,"Caiste en Castigo, te tienes que devolver 2 espacios!","CASTIGO",JOptionPane.ERROR_MESSAGE);
                    Jugador4.setVisible(false);
                    Casilla16.add(Jugador4);
                    Casilla16.validate();
                    Jugador4.setVisible(true);
                    band=true;
                }
                if(np.getInfo().getCasillaVisitada()==18){
                    Jugador4.setVisible(false);
                    Casilla19.add(Jugador4);
                    Casilla19.validate();
                    Jugador4.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(18, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==19){
                    Jugador4.setVisible(false);
                    Casilla20.add(Jugador4);
                    Casilla20.validate();
                    Jugador4.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(19, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==20){
                    Jugador4.setVisible(false);
                    Casilla21.add(Jugador4);
                    Casilla21.validate();
                    Jugador4.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(20, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==21){
                    Jugador4.setVisible(false);
                    Casilla22.add(Jugador4);
                    Casilla22.validate();
                    Jugador4.setVisible(true);
                    j.RelacionCasillaPanel(21, cj.devolverJugador());
                    JOptionPane.showMessageDialog(null,"Has cometido un delito!\nTe vamos a enviar a la CARCEL!","IR A CARCEL",JOptionPane.ERROR_MESSAGE);
                    Jugador4.setVisible(false);
                    Casilla8.add(Jugador4);
                    Casilla8.validate();
                    Jugador4.setVisible(true);
                }
                if(np.getInfo().getCasillaVisitada()==22){
                    Jugador4.setVisible(false);
                    Casilla23.add(Jugador4);
                    Casilla23.validate();
                    Jugador4.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(22, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==23){
                    Jugador4.setVisible(false);
                    Casilla24.add(Jugador4);
                    Casilla24.validate();
                    Jugador4.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(23, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==24){
                    Jugador4.setVisible(false);
                    Casilla25.add(Jugador4);
                    Casilla25.validate();
                    Jugador4.setVisible(true);
                    j.RelacionCasillaPanel(24, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==25){
                    Jugador4.setVisible(false);
                    Casilla26.add(Jugador4);
                    Casilla26.validate();
                    Jugador4.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(25, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==26){
                    Jugador4.setVisible(false);
                    Casilla27.add(Jugador4);
                    Casilla27.validate();
                    Jugador4.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(26, cj.devolverJugador());
                }
                if(np.getInfo().getCasillaVisitada()==27){
                    Jugador4.setVisible(false);
                    Casilla28.add(Jugador4);
                    Casilla28.validate();
                    Jugador4.setVisible(true);
                    if(banderola == true)
                        j.RelacionCasillaPanel(27, cj.devolverJugador());
                }
            }else{
                TipoCasilla temp;
                temp = (TipoCasilla)j.getTablero().get(7);
                if(temp instanceof Carcel){
                    Carcel ctemp = (Carcel) temp;
                    ctemp.SalirdelaCarcel(cj.devolverJugador(), np.getInfo().getCasillaVisitada());
                }
                if(np.getInfo().getCasillaVisitada() == 11){
                    JOptionPane.showMessageDialog(null,"Ha tirado un 4 puede salir de la carcel");
                    Jugador4.setVisible(false);
                    Casilla12.add(Jugador4);
                    Casilla12.validate();
                    Jugador4.setVisible(true);
                }else{
                    if(np.getInfo().getCasillaVisitada() == 12){
                        JOptionPane.showMessageDialog(null,"Ha tirado un 5 puede salir de la carcel");
                        Jugador4.setVisible(false);
                        Casilla13.add(Jugador4);
                        Casilla13.validate();
                        Jugador4.setVisible(true);
                    }else{
                        if(np.getInfo().getCasillaVisitada() == 13){
                            JOptionPane.showMessageDialog(null,"Ha tirado un 6 puede salir de la carcel");
                            Jugador4.setVisible(false);
                            Casilla14.add(Jugador4);
                            Casilla14.validate();
                            Jugador4.setVisible(true);
                        }else{
                            Comprar.setEnabled(false);
                            np.getInfo().setEncarcelado(false);
                            JOptionPane.showMessageDialog(null,"Esperese un turno m�s");
                            if(np.getInfo().getCasillaVisitada()==10){
                                np.getInfo().setCasillasDesplazadas(-3);
                                np.getInfo().setCasillaVisitada(-3);
                            }
                            if(np.getInfo().getCasillaVisitada()==9){
                                np.getInfo().setCasillasDesplazadas(-2);
                                np.getInfo().setCasillaVisitada(-2);
                            }
                            if(np.getInfo().getCasillaVisitada()==8){
                                np.getInfo().setCasillasDesplazadas(-1);
                                np.getInfo().setCasillaVisitada(-1);
                            }
                        }
                        
                    }
                    
                }
            }
        }
    }
    private void ComprarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ComprarActionPerformed
        /*Este m�todo genera la acci�n de comprar para as� modificarle los atributos
         al jugador*/
        banderola = true;
        SetearInfo();
        MoverJugadorTablero();
        SetearInfo();
        JOptionPane.showMessageDialog(null,"Ha comprado comida ! ");
        Comprar.setEnabled(false);
    }//GEN-LAST:event_ComprarActionPerformed
    
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Interfaz().setVisible(true);
                
            }
        });
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BotonBitacora;
    private javax.swing.JPanel Casilla1;
    private javax.swing.JPanel Casilla10;
    private javax.swing.JPanel Casilla11;
    private javax.swing.JPanel Casilla12;
    private javax.swing.JPanel Casilla13;
    private javax.swing.JPanel Casilla14;
    private javax.swing.JPanel Casilla15;
    private javax.swing.JPanel Casilla16;
    private javax.swing.JPanel Casilla17;
    private javax.swing.JPanel Casilla18;
    private javax.swing.JPanel Casilla19;
    private javax.swing.JPanel Casilla2;
    private javax.swing.JPanel Casilla20;
    private javax.swing.JPanel Casilla21;
    private javax.swing.JPanel Casilla22;
    private javax.swing.JPanel Casilla23;
    private javax.swing.JPanel Casilla24;
    private javax.swing.JPanel Casilla25;
    private javax.swing.JPanel Casilla26;
    private javax.swing.JPanel Casilla27;
    private javax.swing.JPanel Casilla28;
    private javax.swing.JPanel Casilla3;
    private javax.swing.JPanel Casilla4;
    private javax.swing.JPanel Casilla5;
    private javax.swing.JPanel Casilla6;
    private javax.swing.JPanel Casilla7;
    private javax.swing.JPanel Casilla8;
    private javax.swing.JPanel Casilla9;
    private javax.swing.JButton Comprar;
    private javax.swing.JLabel Dado1;
    private javax.swing.JLabel Dado2;
    private javax.swing.JLabel Dado3;
    private javax.swing.JLabel Dado4;
    private javax.swing.JLabel Dado5;
    private javax.swing.JLabel Dado6;
    private javax.swing.JButton Iniciar;
    private javax.swing.JLabel Jug1Turno;
    private javax.swing.JLabel Jug2Turno;
    private javax.swing.JLabel Jug3Turno;
    private javax.swing.JLabel Jug4Turno;
    private javax.swing.JLabel Jugador1;
    private javax.swing.JLabel Jugador2;
    private javax.swing.JLabel Jugador3;
    private javax.swing.JLabel Jugador4;
    private javax.swing.JTextField NomJug1;
    private javax.swing.JTextField NomJug2;
    private javax.swing.JTextField NomJug3;
    private javax.swing.JTextField NomJug4;
    private javax.swing.JPanel PanelInfo;
    private javax.swing.JPanel PanelPrincipal;
    private javax.swing.JPanel PanelTablero;
    private javax.swing.JRadioButton RadioJug1;
    private javax.swing.JRadioButton RadioJug2;
    private javax.swing.JRadioButton RadioJug3;
    private javax.swing.JRadioButton RadioJug4;
    private javax.swing.JButton TurnoSiguiente;
    private javax.swing.JTextField infoCasillaActual;
    private javax.swing.JTextField infoColor;
    private javax.swing.JTextField infoDinero;
    private javax.swing.JTextField infoNom;
    private javax.swing.JTextField infoNum;
    private javax.swing.JTextField infoPeso;
    private javax.swing.JTextField infoVueltas;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel44;
    private javax.swing.JLabel jLabel45;
    private javax.swing.JLabel jLabel46;
    private javax.swing.JLabel jLabel47;
    private javax.swing.JLabel jLabel48;
    private javax.swing.JLabel jLabel49;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel50;
    private javax.swing.JLabel jLabel51;
    private javax.swing.JLabel jLabel52;
    private javax.swing.JLabel jLabel53;
    private javax.swing.JLabel jLabel54;
    private javax.swing.JLabel jLabel55;
    private javax.swing.JLabel jLabel56;
    private javax.swing.JLabel jLabel57;
    private javax.swing.JLabel jLabel58;
    private javax.swing.JLabel jLabel59;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel60;
    private javax.swing.JLabel jLabel61;
    private javax.swing.JLabel jLabel62;
    private javax.swing.JLabel jLabel63;
    private javax.swing.JLabel jLabel64;
    private javax.swing.JLabel jLabel65;
    private javax.swing.JLabel jLabel66;
    private javax.swing.JLabel jLabel67;
    private javax.swing.JLabel jLabel68;
    private javax.swing.JLabel jLabel69;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel70;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel32;
    private javax.swing.JPanel jPanel33;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JLabel labelGanador1;
    private javax.swing.JLabel labelGanador2;
    private javax.swing.JLabel labelGanador3;
    private javax.swing.JLabel labelGanador4;
    private javax.swing.JLabel labelJuego;
    private javax.swing.JButton lanzarDado;
    private javax.swing.JMenuItem mensaje;
    private javax.swing.JMenuItem salir;
    // End of variables declaration//GEN-END:variables
    
}
