/*Esta clase muestra un dibujo de bienvenida*/
package Ejecutables;
import Juego.*;

public class Bienvenida extends javax.swing.JFrame {
    
    private hiloProgressBar barraPro;
    private Interfaz inicio;
    static Tablero t;
    
    public Bienvenida(Tablero t) {
        t = t;
        this.setLocation(250, 250);
        barraPro = new hiloProgressBar();
        inicio = new Interfaz();
        initComponents();
        barraPro.setJProgressBarInicio(this.barraProgreso);
    }
    
    // <editor-fold defaultstate="collapsed" desc=" Generated Code ">//GEN-BEGIN:initComponents
    private void initComponents() {
        jLabel1 = new javax.swing.JLabel();
        barraProgreso = new javax.swing.JProgressBar();

        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/progressBar.jpg")));
        jLabel1.setText("VENTANA DE ENTRADA");
        jLabel1.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0)));
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 430, 260));

        barraProgreso.setBackground(new java.awt.Color(255, 255, 255));
        barraProgreso.setForeground(new java.awt.Color(166, 166, 208));
        barraProgreso.setBorder(new javax.swing.border.BevelBorder(javax.swing.border.BevelBorder.LOWERED));
        barraProgreso.setStringPainted(true);
        barraProgreso.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                barraProgresoStateChanged(evt);
            }
        });

        getContentPane().add(barraProgreso, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 220, 130, -1));

        pack();
    }
    // </editor-fold>//GEN-END:initComponents

    private void barraProgresoStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_barraProgresoStateChanged
/*Este m�todo no llama a la ventana principal hasta que la
         barra de progreso sea igual a 99*/
        if(barraProgreso.getValue()==99){
            inicio.setVisible(true);
            this.setVisible(false);
        }
    }//GEN-LAST:event_barraProgresoStateChanged
        
    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        barraPro.start();
    }//GEN-LAST:event_formWindowOpened
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JProgressBar barraProgreso;
    private javax.swing.JLabel jLabel1;
    // End of variables declaration//GEN-END:variables
    
}
