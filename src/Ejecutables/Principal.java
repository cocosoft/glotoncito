/*Esta clase llama a la clase Bienvenida para que
 esta llame a la clase principal y el programa se ejecute*/
package Ejecutables;
import Juego.*;
import java.io.*;
import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;
import javax.sound.sampled.UnsupportedAudioFileException;

public class Principal {
    static Tablero s = new Tablero();
    
    public Principal(Tablero s){
        s = s;
    }
    
    
    private static void rawplay(AudioFormat targetFormat,
                     AudioInputStream din)
   throws IOException, LineUnavailableException
{
  byte[] data = new byte[4096];
  SourceDataLine line = getLine(targetFormat);
  if (line != null)
  {
    line.start();
    int nBytesRead = 0, nBytesWritten = 0;
    while (nBytesRead != -1)
    {
        nBytesRead = din.read(data, 0, data.length);
        if (nBytesRead != -1)
            nBytesWritten = line.write(data, 0, nBytesRead);
    }
    line.drain();
    line.stop();
    line.close();
    din.close();
  }
}

private static SourceDataLine getLine(AudioFormat audioFormat)
    throws LineUnavailableException
{
  SourceDataLine res = null;
  DataLine.Info info =
    new DataLine.Info(SourceDataLine.class, audioFormat);
  res = (SourceDataLine) AudioSystem.getLine(info);
  res.open(audioFormat);
  return res;
}
    

    public static void main(String[] args) {
        Bienvenida inicio = new Bienvenida(s);
         inicio.setVisible(true);

// try{   
//File file = new File("jugo.mp3");
//AudioFileFormat baseFileFormat = null;
//baseFileFormat = AudioSystem.getAudioFileFormat(file);
//AudioFileFormat.Type type = baseFileFormat.getType();      
//AudioInputStream in= AudioSystem.getAudioInputStream(file);
//AudioInputStream din = null;
//AudioFormat baseFormat = in.getFormat();
//baseFormat = baseFileFormat.getFormat();
//float frequency = baseFormat.getSampleRate();   
//AudioFormat decodedFormat = new AudioFormat(AudioFormat.Encoding.PCM_SIGNED,  baseFormat.getSampleRate(),  16,   baseFormat.getChannels(),                    baseFormat.getChannels() * 2,                    baseFormat.getSampleRate(),                    false);
//din = AudioSystem.getAudioInputStream(decodedFormat, in);
//rawplay(decodedFormat, din); 
//in.close();
// }catch(UnsupportedAudioFileException e1){}
//  catch(IOException e2){}
//        catch(LineUnavailableException e3){} 
      
    }
    
}
