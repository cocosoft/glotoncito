package Jugador;
public class Bitacora {
    
    private String casillasVisitadas;
    private int pesoAcumulado;
    
    public Bitacora(){
        this.setCasillasVisitadas("");
        this.setPesoAcumulado(0);
    }
    
    public Bitacora(String casVis, int peso){
        this.setCasillasVisitadas(casVis);
        this.setPesoAcumulado(peso);
    }

    public String getCasillasVisitadas() {
        return casillasVisitadas;
    }

    public void setCasillasVisitadas(String casillasVisitadas) {
        this.casillasVisitadas = casillasVisitadas;
    }

    public int getPesoAcumulado() {
        return pesoAcumulado;
    }

    public void setPesoAcumulado(int pesoAcumulado) {
        this.pesoAcumulado = pesoAcumulado;
    }
 }
