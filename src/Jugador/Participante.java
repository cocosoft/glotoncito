package Jugador;
import EstructurasDinamicas.ListaBitacora;
import java.awt.Color;
import javax.swing.*;


public class Participante {


    private String nombre;
    private int dinero;
    private int casillasDesplazadas;
    private int peso;
    private int numJugador;
    private int casillaVisitada;
    private String color;
    private boolean encarcelado;
    private int contador;
    private Bitacora bitacoraJ;
    private ListaBitacora ListaBitacora;
    private int numeroV;
    private boolean ganador;


    
    public Participante(){
        this.setListaBitacora(new ListaBitacora());
        setNombre("");
        setDinero(0);
        setCasillasDesplazadas(0);
        setPeso(0);
        setNumJugador(0);
        setCasillaVisitada(0);
        setColor("");
        setEncarcelado(false); 
        setContador(1);
        this.setBitacoraJ(new Bitacora());
        setNumeroV(0);
        setGanador(false);
        
    }
    
    public Participante(String nom,int dinero,int casillasDesp,int peso,int numjug,int casillaVis, String col,boolean encarcelado,int cont,int numV){
    
        this.setNombre(nom);
        this.setDinero(dinero);
        this.setCasillasDesplazadas(casillasDesp);
        this.setPeso(peso);
        this.setNumJugador(numjug);
        this.setCasillaVisitada(casillaVis);
        this.setColor(col);
        this.setEncarcelado(encarcelado);
        this.setContador(cont);
        this.setNumeroV(numV);
        this.setBitacoraJ(new Bitacora());
        this.setListaBitacora(new ListaBitacora());
    }
    public void llenarBitacora(){
        int casilla= getCasillaVisitada();
        int peso = getPeso();
        String cadena = String.valueOf(casilla);
        getListaBitacora().InsertarInicio(cadena,peso); 
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getDinero() {
        return dinero;
    }

    public void setDinero(int dinero) {
        this.dinero = dinero;
    }


    public int getCasillasDesplazadas() {
        return casillasDesplazadas;
    }

    public void setCasillasDesplazadas(int casillasDesplazadas) {
        this.casillasDesplazadas += casillasDesplazadas;
    }

    public int getPeso() {
        return peso;
    }

    public void setPeso(int peso) {
        this.peso = peso;
    }

    public int getNumJugador() {
        return numJugador;
    }

    public void setNumJugador(int numJugador) {
        this.numJugador = numJugador;
    }

    public int getCasillaVisitada() {
        return casillaVisitada;
    }

    public void setCasillaVisitada(int casillaVisitada) { 
        /*Valida la casilla para saber en cu�l casilla est� el jugador*/
        casillaVisitada = casillasDesplazadas;
        this.contador=1;
        if(casillaVisitada>27){
            this.casillaVisitada = casillasDesplazadas-28; 
            this.contador=2;
            
             
            if (casillaVisitada>56){
                this.casillaVisitada=casillasDesplazadas-57;
                this.contador=3;
                contador=3;
                    if (casillaVisitada>85){
                        this.casillaVisitada=casillasDesplazadas-86;
                        this.contador=4;
                    }
            }
        }
                
        else{
        this.casillaVisitada = casillasDesplazadas;
        contador=1;
        }
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public boolean isEncarcelado() {
        return encarcelado;
    }

    public void setEncarcelado(boolean encarcelado) {
        this.encarcelado = encarcelado;
    }

    public int getContador() {
        return contador;
    }

    public void setContador(int contador) {
        this.contador = contador;
    }

    public int getNumeroV() {
        return numeroV;
    }

    public void setNumeroV(int numeroV) {
        if(numeroV==4){
            setGanador(true);
        }
        this.numeroV=numeroV;
    }

    public boolean isGanador() {
        return ganador;
    }

    public void setGanador(boolean ganador) {
        this.ganador = ganador;
    }
    
    public void pasarPorInicio(int i, boolean b){
        /*Le aumenta el dinero al jugador*/
        i = contador;
        if(i==2&&b==false){
            JOptionPane.showMessageDialog(null,"Has pasado por la casilla de Inicio.\nRecibes un premio de 2000!","PREMIO",JOptionPane.INFORMATION_MESSAGE);
            this.dinero+=2000;
            b=true;
        }
        
        if(i==3&&b==false){
            JOptionPane.showMessageDialog(null,"Has pasado por la casilla de Inicio.\nRecibes un premio de 2000!","PREMIO",JOptionPane.INFORMATION_MESSAGE);
            this.dinero+=2000;
            b=true;
        }
        if(i==4&&b==false){
            JOptionPane.showMessageDialog(null,"Has pasado por la casilla de Inicio.\nRecibes un premio de 2000!","PREMIO",JOptionPane.INFORMATION_MESSAGE);
            this.dinero+=2000;
            b=true;
        }
    }
    
    
    
    
    
    public void desplegarbitacora(){
             getListaBitacora().Visualizar();
        }

    public Bitacora getBitacoraJ() {
        return bitacoraJ;
    }

    public void setBitacoraJ(Bitacora bitacoraJ) {
        this.bitacoraJ = bitacoraJ;
    }

    public ListaBitacora getListaBitacora() {
        return ListaBitacora;
    }

    public void setListaBitacora(ListaBitacora ListaBitacora) {
        this.ListaBitacora = ListaBitacora;
    }
    
    
    
    
    
    
    

 

 }
