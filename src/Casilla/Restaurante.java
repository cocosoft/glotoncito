package Casilla;
/*Esta clase extiende los atributos de la clase TipoCasilla*/
import EstructurasDinamicas.NodoParticipante;
import Jugador.*;

public class Restaurante extends TipoCasilla {
    
    private String nombre;
    private String comida;
    private int peso;
    private int precProducto;
    
    public Restaurante(){
        this.setNombre("");
        this.setComida("");
        this.setPeso(0);
        this.setPrecProducto(0);
    }
    public Restaurante(boolean ocupado,String mensage,int indice,String nombre,String comida,int peso,int precProducto){
        
        super(ocupado, mensage, indice);
        this.setNombre(nombre);
        this.setComida(comida);
        this.setPeso(peso);
        this.setPrecProducto(precProducto);
    }
    public void CambiarStats(NodoParticipante p){
         /*Este m�todo modifica los atributos del participante si ha ca�do en
          una casilla restaurante*/
        int peso = p.getInfo().getPeso();
        p.getInfo().setPeso(peso+getPeso());
        int dineroDisponible = p.getInfo().getDinero();
        p.getInfo().setDinero(dineroDisponible-getPrecProducto());
        p.getInfo().llenarBitacora();
    }
    public String getNombre() {
        return nombre;
    }
    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    public String getComida() {
        return comida;
    }
    
    public void setComida(String comida) {
        this.comida = comida;
    }
    
    public int getPeso() {
        return peso;
    }
    
    public void setPeso(int peso) {
        this.peso = peso;
    }
    
    public int getPrecProducto() {
        return precProducto;
    }
    
    public void setPrecProducto(int precProducto) {
        this.precProducto = precProducto;
    }
}
