package Casilla;
/*Esta clase extiende los atributos de la clase TipoCasilla*/
import EstructurasDinamicas.NodoParticipante;
import Jugador.Participante;

public class IrACarcel extends TipoCasilla {
    
    public IrACarcel(){
    }
    public IrACarcel(boolean ocupado,String mensage,int indice){
        super(ocupado, mensage, indice);
    }
    public void MoveraCarcel(NodoParticipante p){
        /*Este m�todo modifica los atributos de las casillas del participante
         para que se devuelva en el tablero*/
        p.getInfo().setCasillasDesplazadas(-14);
        p.getInfo().setCasillaVisitada(8);
        p.getInfo().setEncarcelado(true);
        p.getInfo().llenarBitacora();
    }
}
