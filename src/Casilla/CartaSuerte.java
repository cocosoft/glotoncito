package Casilla;
/*Esta clase extiende los atributos de la clase TipoCasilla*/

import java.io.Serializable;

public class CartaSuerte implements Serializable {
    
    private String mensage;
    
    
    public CartaSuerte(String mensage){
        setMensage(mensage);
    }
    
    public String getMensage() {
        return mensage;
    }
    
    public void setMensage(String mensage) {
        this.mensage = mensage;
    }
}
