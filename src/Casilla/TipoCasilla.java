package Casilla;
public class TipoCasilla {
    
    private boolean ocupado;
    private String mensajeCasilla;
    private int indice;
    
    public TipoCasilla(){
        this.setOcupado(false);
        this.setMensajeCasilla("");
        this.setIndice(0);
        
    }
    public TipoCasilla(boolean ocupado,String mensage,int indice){
        this.setOcupado(ocupado);
        this.setMensajeCasilla(mensage);
        this.setIndice(indice);
    }
    
    public boolean isOcupado() {
        return ocupado;
    }
    
    public void setOcupado(boolean ocupado) {
        this.ocupado = ocupado;
    }
    
    public String getMensajeCasilla() {
        return mensajeCasilla;
    }
    
    public void setMensajeCasilla(String mensajeCasilla) {
        this.mensajeCasilla = mensajeCasilla;
    }
    
    public int getIndice() {
        return indice;
    }
    
    public void setIndice(int indice) {
        this.indice = indice;
    }
    
    
}
