package Casilla;
/*Esta clase extiende los atributos de la clase TipoCasilla*/
import EstructurasDinamicas.NodoParticipante;
import Juego.*;
import Jugador.Participante;

public class Carcel extends TipoCasilla{
    
    public Carcel() {
    }
    public Carcel(boolean ocupado,String mensage,int indice){
        super(ocupado, mensage, indice);
        
    }
    public void SalirdelaCarcel(NodoParticipante p,int dato){
        /*Este m�todo valida lo que lanza el jugador para salir o no de la carcel*/
        Tablero tab = new Tablero();
        if(dato==11){
            p.getInfo().setEncarcelado(false);
        }
        if(dato==12){
            p.getInfo().setEncarcelado(false);
        }
        if(dato==13){
            p.getInfo().setEncarcelado(false);
        }
        if(dato<11){
            p.getInfo().setCasillaVisitada(8);
            p.getInfo().setEncarcelado(true);
        }
        p.getInfo().llenarBitacora();
    }
}
