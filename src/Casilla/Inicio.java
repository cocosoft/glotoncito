package Casilla;
/*Esta clase extiende los atributos de la clase TipoCasilla*/
import EstructurasDinamicas.NodoParticipante;
import Jugador.*;
public class Inicio extends TipoCasilla {
    
    private int premio;
    
    public Inicio(){
        this.setPremio(2000);
        
    }
    public Inicio(boolean ocupado,String mensage,int indice){
        super(ocupado,mensage,indice);
    }
    public void AsignarDinerdoJugador(NodoParticipante p){
        /*Este m�todo le modifica el atributo de dinero del jugador*/
        int uno = p.getInfo().getDinero();
        p.getInfo().setDinero(uno+getPremio());
        p.getInfo().llenarBitacora();
    }
    
    public int getPremio() {
        return premio;
    }
    
    public void setPremio(int premio) {
        this.premio = premio;
    }
}
