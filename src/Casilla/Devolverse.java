package Casilla;
import Jugador.*;
import EstructurasDinamicas.*;
/*Esta clase extiende los atributos de la clase TipoCasilla*/

public class Devolverse extends TipoCasilla {
    
    
    
    public Devolverse(){
    }
    public Devolverse(boolean ocupado,String mensage,int indice){
        super(ocupado, mensage, indice);
    }
    public void DevolverJugador(NodoParticipante p){
        /*Este m�todo devuelve al jugador 2 espacios en el tablero*/
        p.getInfo().setCasillasDesplazadas(-2);
        p.getInfo().setCasillaVisitada(-2);
        p.getInfo().llenarBitacora();
        
    }
}
