package Casilla;
import EstructurasDinamicas.*;
/*Esta clase extiende los atributos de la clase TipoCasilla*/
public class DePaso extends TipoCasilla {
    
    public DePaso(){
        
    }
    public DePaso(boolean ocupado,String mensage,int indice){
        super(ocupado, mensage, indice);
    }
    public void ModDatos(NodoParticipante p){
        /*Este m�todo s�lo llama al llenar bitacora para que se almacena la
         jugada del jugador*/
        p.getInfo().llenarBitacora();
    }
}
