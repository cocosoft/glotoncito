package Juego;
/*Esta clase realiza las acciones principales del juego*/
import Casilla.Carcel;
import Casilla.DePaso;
import Casilla.Devolverse;
import Casilla.Inicio;
import Casilla.IrACarcel;
import Casilla.Restaurante;
import Casilla.Suerte;
import Casilla.TipoCasilla;
import EstructurasDinamicas.*;
import Jugador.*;
import java.io.*;
import java.util.*;
import javax.swing.*;

public class Tablero implements Serializable {
    
    private Participante Jugador;
    private ColaJugadores ColaJ;
    private Vector tablero;
    private Suerte suerte;
    private PilaCartasSuerte pila;
    private String records[];
    private int contRecord;
    
    public Tablero(){
        this.Jugador = null;
        this.ColaJ = null;
        this.suerte=null;
        this.setTablero(new Vector());
        this.pila=new PilaCartasSuerte();
        records = new String[50];
        for (int i =0;i<records.length;i++){
            records[i]=null;
        }
        contRecord=0;
    }
    
    public void llenarPilaSuerte(){
        /*Llena toda la pila de cartas de la suerte*/
        pila.Push("Perdiste 2lb de peso");
        pila.Push("Ganaste 20lb de peso");
        pila.Push("Ganaste la comilona 400 de dinero");
        pila.Push("Perdiste la comilona 200 de dinero");
        pila.Push("Ganaste 20lb de peso");
        pila.Push("Perdiste 2lb de peso");
        pila.Push("Perdiste la comilona 200 de dinero");
        pila.Push("Ganaste la comilona 400 de dinero");
        pila.Push("No tuviste suerte");
        pila.Push("Que tengas un buen dia!");
        pila.Push("Perdiste 2lb de peso");
        pila.Push("Ganaste 20lb de peso");
        pila.Push("Ganaste la comilona 400 de dinero");
        pila.Push("Perdiste la comilona 200 de dinero");
        pila.Push("Ganaste 20lb de peso");
        pila.Push("Perdiste 2lb de peso");
        pila.Push("Perdiste la comilona 200 de dinero");
        pila.Push("Ganaste la comilona 400 de dinero");
        pila.Push("No tuviste suerte");
        pila.Push("Que tengas un buen dia!");
        pila.Push("Perdiste 2lb de peso");
        pila.Push("Ganaste 20lb de peso");
        pila.Push("Ganaste la comilona 400 de dinero");
        pila.Push("Perdiste la comilona 200 de dinero");
        pila.Push("Ganaste 20lb de peso");
        pila.Push("Perdiste 2lb de peso");
        pila.Push("Perdiste la comilona 200 de dinero");
        pila.Push("Ganaste la comilona 400 de dinero");
        pila.Push("No tuviste suerte");
        pila.Push("Que tengas un buen dia!");
    }
    public void GuardarCartasDeLaSuerte(String nombreArchivo){
        /*Guarda la Pila en un archivo*/
        pila.guardar(nombreArchivo);
    }
    public void CargarArchivo(String nombrearchivo){
        /*Carga la pila en un archivo*/
        pila.CargarArchivo(nombrearchivo);
    }
    
    public void SacarCartadelaSuerte(NodoParticipante p){
        /*Saca carta de la suerte mediante la accion pop()*/
        String mensage = pila.PoP();
        JOptionPane.showMessageDialog(null,mensage,"Carta de la Suerte",JOptionPane.INFORMATION_MESSAGE);
        if(mensage.equalsIgnoreCase("Perdiste 2lb de peso")){
            p.getInfo().setPeso(p.getInfo().getPeso()-2);
        }
        if(mensage.equalsIgnoreCase("Ganaste 20lb de peso")){
            p.getInfo().setPeso(p.getInfo().getPeso()+20);
        }
        if(mensage.equalsIgnoreCase("Ganaste la comilona 400 de dinero")){
            p.getInfo().setDinero(p.getInfo().getDinero()+400);
        }
        if(mensage.equalsIgnoreCase("Perdiste la comilona 200 de dinero")){
            p.getInfo().setDinero(p.getInfo().getDinero()-200);
        }
        p.getInfo().llenarBitacora();
    }
    
    
    
    public int LanzarDado(){
        /*Genera un n�mero de 1 a 6 aleatoriamente*/
        int dado = 0;
        dado = (int)((Math.random()*6)+1);
        return dado;
    }
    public void RelacionCasillaPanel(int indice,NodoParticipante participante){
        /*Esta casilla hace relaci�n recibiendo un �ndice y accsesando a un vector
         para saber en que casilla se encuentra y generar cierta operaci�n*/
        TipoCasilla temp;
        temp = (TipoCasilla)tablero.get(indice);
        NodoParticipante partemp = null;
        if(temp instanceof Carcel){
            JOptionPane.showMessageDialog(null,"De Visita en la carcel","CARCEL", JOptionPane.WARNING_MESSAGE);
        }
        if(temp instanceof DePaso){
            DePaso dptemp = (DePaso)temp;
            dptemp.ModDatos(participante);
        }
        if(temp instanceof Devolverse){
            Devolverse dtemp = (Devolverse)temp;
            dtemp.DevolverJugador(participante);
        }
        if(temp instanceof Inicio){
            Inicio itemp = (Inicio)temp;
            itemp.AsignarDinerdoJugador(participante);
        }
        if(temp instanceof IrACarcel){
            IrACarcel irtemp = (IrACarcel)temp;
            irtemp.MoveraCarcel(participante);
        }
        
        if(temp instanceof Restaurante){
            Restaurante rtemp = (Restaurante)temp;
            rtemp.CambiarStats(participante);
        }
        
        if(temp instanceof Suerte){
            Suerte stemp = (Suerte)temp;
            SacarCartadelaSuerte(participante);
        }
    }
    
    public void CrearJugador(String nom,int dinero,int
            casillasDesp,int peso,int numjug,int casillaVis, String col,boolean encarcelado,int cont,int numV){
        /*Crea un jugador agregandolo a la cola */
        ColaJ.agregar(nom, dinero,  casillasDesp, peso,
                numjug, casillaVis, col, encarcelado, cont, numV);
    }
    
    public void CrearTablero(){
        /*Crea las casillas polim�rficamente y las agrega a un vector*/
        TipoCasilla ini = new Inicio(false,"Has ganado 2000", 0);
        tablero.addElement(ini);
        TipoCasilla res1 = new Restaurante(false, "Desea comprar una costilla", 1, "Cuchifrito", "Costilla", 2, 100);
        tablero.addElement(res1);
        TipoCasilla res2 = new Restaurante(false, "Desea comprar una pupusa", 2, "El jardin de tita", "Pupusa", 2, 100);
        tablero.addElement(res2);
        TipoCasilla res3 = new Restaurante(false, "Desea comprar una sopa", 3, "Do�a lela", "Sopa", 2, 100);
        tablero.addElement(res3);
        TipoCasilla dev1 = new Devolverse(false, "Devualvase 2 casillas", 4);
        tablero.addElement(dev1);
        TipoCasilla res4 = new Restaurante(false, "Desea comprar una cajeta", 5, "Twinky", "Cajeta", 3, 200);
        tablero.addElement(res4);
        TipoCasilla res5 = new Restaurante(false, "Desea comprar un helado", 6,"Pops", "Helado", 3, 200);
        tablero.addElement(res5);
        TipoCasilla carcel = new Carcel(false, "Estas en la carcel", 7);
        tablero.addElement(carcel);
        TipoCasilla res6 = new Restaurante(false, "Desea comprar un churro", 8, "Manolos", "Churro", 4, 300);
        tablero.addElement(res6);
        TipoCasilla res7 = new Restaurante(false, "Desea comprar unas papas", 9, "Karey", "Papas", 4, 300);
        tablero.addElement(res7);
        TipoCasilla res8 = new Restaurante(false, "Desea comprar un soda", 10, "Serex", "Soda", 4, 300);
        tablero.addElement(res8);
        TipoCasilla suerte1 = new Suerte(false, "Saca carta de la suerte", 11);
        tablero.addElement(suerte1);
        TipoCasilla res9 = new Restaurante(false, "Desea comprar chirulos", 12, "Ferdis", "Chirulos", 5, 400);
        tablero.addElement(res9);
        TipoCasilla res10 = new Restaurante(false, "Desea comprar un pasta", 13, "Mante", "Pasta", 5, 400);
        tablero.addElement(res10);
        TipoCasilla depaso = new DePaso(false, "Solo de paso", 14);
        tablero.addElement(depaso);
        TipoCasilla res11 = new Restaurante(false, "Desea comprar un confite", 15, "CandyShop", "Confite", 6, 500);
        tablero.addElement(res11);
        TipoCasilla res12 = new Restaurante(false, "Desea comprar una pizza", 16, "PizzaHut", "Pizza", 6, 500);
        tablero.addElement(res12);
        TipoCasilla dev2 = new Devolverse(false, "Devualvase 2 casillas", 17);
        tablero.addElement(dev2);
        TipoCasilla res13 = new Restaurante(false, "Desea comprar un pollazo",18, "Pollo sus amigos", "Pollazo", 7, 600);
        tablero.addElement(res13);
        TipoCasilla res14 = new Restaurante(false, "Desea comprar un emparedado", 19, "Entre Pans", "Emparedado", 7, 600);
        tablero.addElement(res14);
        TipoCasilla res15 = new Restaurante(false, "Desea comprar un queque", 20, "Spoon", "Queque", 7, 600);
        tablero.addElement(res15);
        TipoCasilla irACarcel = new IrACarcel(false, "Vete a la carcel", 21);
        tablero.addElement(irACarcel);
        TipoCasilla res16 = new Restaurante(false, "Desea comprar un taco al pastor", 22, "Huaraches", "Taco al Pastor", 8, 700);
        tablero.addElement(res16);
        TipoCasilla res17 = new Restaurante(false, "Desea comprar un antipasto", 23, "La fabrica", "AntiPasto", 8, 700);
        tablero.addElement(res17);
        TipoCasilla suerte2 = new Suerte(false, "Escoge una carta de la suerte", 24);
        tablero.addElement(suerte2);
        TipoCasilla res18 = new Restaurante(false, "Desea comprar un sushi",25, "Ichiban", "sushi", 8, 700);
        tablero.addElement(res18);
        TipoCasilla res19 = new Restaurante(false, "Desea comprar un chporipan", 26, "Fosa Argentina", "Choripan", 8, 700);
        tablero.addElement(res19);
        TipoCasilla res20 = new Restaurante(false, "Desea comprar pollo chino", 27, "Shian-Sen", "Pollo chino", 8, 700);
        tablero.addElement(res20);
        
    }
    public Participante getJugador() {
        return Jugador;
    }
    public void setJugador(Participante Jugador) {
        this.Jugador = Jugador;
    }
    public ColaJugadores getColaJ() {
        return ColaJ;
    }
    public void setColaJ(ColaJugadores ColaJ) {
        this.ColaJ = ColaJ;
    }
    public Vector getTablero() {
        return tablero;
    }
    public void setTablero(Vector tablero) {
        this.tablero = tablero;
    }
    
    public void guardarRecord(){
        /*Guarda en un archivo los records de los jugadores*/
        ObjectOutputStream escribir = null;
        
        try{
            
            escribir = new ObjectOutputStream(new FileOutputStream("records.dat"));
            
        }catch (IOException e){
            JOptionPane.showMessageDialog(null,"ERROR DE CREACION DEL ARCHIVO","ERROR",JOptionPane.ERROR_MESSAGE);
            
        }
        
        try{
            
            escribir.writeObject(records);
            
        }catch (IOException e2){
            JOptionPane.showMessageDialog(null,"ERROR DE ESCRITURA EN EL ARCHIVO","ERROR",JOptionPane.ERROR_MESSAGE);
        }
        
        try{
            escribir.close();
            
        }catch (IOException e3){
            JOptionPane.showMessageDialog(null,"ERROR AL CERRAR ARCHIVO","ERROR",JOptionPane.ERROR_MESSAGE);
        }
        
    }
    
    
    
    
    public void cargarRecord(){
        /*Carga en un archivo los records de los jugadores*/
        ObjectInputStream leer = null;
        
        try {
            leer = new ObjectInputStream(new FileInputStream("records.dat"));
        }catch (IOException e4){}
        
        try{
            records  = (String [])leer.readObject();
            
            
        }catch (IOException e5){} catch(ClassNotFoundException e6){}
        
        
    }
    
    
    public void desplegarRecord(){
        /*Despliega los records de los jugadores que han ganado*/
        int cont=0;
        JTextArea a = new JTextArea(20,20 );
        a.append("\n");
        while(cont<50){
            if(records[cont]!=null){
                a.append("\n"+records[cont]);
                a.append("\n********************");
                cont++;
            } else{cont++;}
        }
        JOptionPane.showMessageDialog(null,a,"REGISTRO DE RECORDS",JOptionPane.INFORMATION_MESSAGE);
    }
    
    
    public void ingresarRecord(NodoParticipante p){
        /*Ingresa en un arreglo los records de los jugadores*/
        boolean bandera = false;
        String mensaje="";
        mensaje+="\n";
        mensaje=p.getInfo().getNombre()+"\nPeso: "+String.valueOf(p.getInfo().getPeso());
        
        while((contRecord<50)&&(bandera==false)){
            
            if(records[contRecord]==null){
                records[contRecord]=mensaje;
                bandera=true;
                contRecord++;
            }else{
                contRecord++;}
            
        }
    }
    
    
    
    
    
    
    
    
}
